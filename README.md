# Git UI

Before doing the steps, you need to install virtualenv

pip3 install virtualenv

## Install packages

1. Start and activate environment

        virtualenv env
        
        source env/bin/activate

1. Run the requirements

        pip3 install -r requirements.txt

1. Run the application

        python3 src/app/main.py

## To test GitRepo
To use GitRepo class, you can clone GitUiTest repo for testing
> https://gitlab.com/intouchKey/gituitest

## Debugging
If you have a problem with libraries:

1. Activate virtual environment

        virtualenv env (FOR MAC)
        \env\Scripts\activate.bat (FOR WINDOW)

1. Run the requirements

        pip3 install -r requirements.txt

After doing this, try running the app again by:

'''
python src/app/main.py

or 

python3 src/app/main.py
'''