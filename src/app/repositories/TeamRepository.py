import transaction

from entities.Team import Team

DEBUG = False

class TeamRepository:
    def __init__(self):
        pass

    def saveTeam(self, name, password, user):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        newTeam = Team(name, password)
        newTeam.addUser(user)

        try: 
            DatabaseConnection.root.teams[name]

            if (DEBUG): print("Team not available")

            return dict({ "msg": "Team name not available", "error": True })
        except: pass

        DatabaseConnection.root.teams[name] = newTeam
        transaction.commit()

        if (DEBUG): print("Team created")

        return dict({ "msg": "Team created", "error": False })

    def findTeamByTeamName(self, teamName):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        try: 
            team = DatabaseConnection.root.teams[teamName]

            if (DEBUG): print("Team found")

            return dict({ "msg": "Team found", "result": team, "error": False })
        except: pass

        if (DEBUG): print("Team not found")

        return dict({ "msg": "Team not found", "error": True })

    def deleteTeam(self, teamName):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()
        
        try:
            del DatabaseConnection.root.teams[teamName]
            transaction.commit()

            return dict({"msg": "Team is deleted successfully", "error": False})
        except:
            return dict({"msg": "Can't delete team", "error": True})

    def commitChanges(self, team):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()
        
        transaction.commit()

    def getDatabaseConnection(self):
        from modules import DatabaseConnection
        global DatabaseConnection