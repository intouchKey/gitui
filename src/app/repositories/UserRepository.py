import transaction

from entities.User import User

DEBUG = False

class UserRepository:
    def __init__(self):
        pass

    def sync(self):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

    def saveUser(self, username, email):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        newUser = User(username, email)

        try: 
            DatabaseConnection.root.users[username]

            if (DEBUG): print("Username not available")

            return dict({ "msg": "Username not available", "error": True })
        except: pass

        try:
            DatabaseConnection.root.accounts[username]
            DatabaseConnection.root.users[username] = newUser
            transaction.commit()

            if (DEBUG): print("User created")

            return dict({ "msg": "User created", "error": False })
        except:
            return dict({ "msg": "User not found", "error": True })
    
    def findUserByUsername(self, username):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        try: 
            user = DatabaseConnection.root.users[username]

            if (DEBUG): print("User found")

            return dict({ "user": user, "error": False })
        except: pass

        if (DEBUG): print("User not found")

        return dict({ "msg": "User not found", "error": True })
    
    def commitChanges(self, user):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        transaction.commit()

    def getDatabaseConnection(self):
        from modules import DatabaseConnection
        global DatabaseConnection
