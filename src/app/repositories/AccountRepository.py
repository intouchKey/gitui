import transaction

from entities.Account import Account

DEBUG = False

class AccountRepository:
    def __init__(self):
        pass

    def saveAccount(self, username, password, email, verificationCode):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        newAccount = Account(username, password, email, verificationCode)

        try: 
            DatabaseConnection.root.accounts[username]

            if (DEBUG): print("Username not available")

            return dict({ "msg": "Username not available", "error": True })
        except: pass

        try:
            DatabaseConnection.root.emails[email]

            return dict({ "msg": "Email not available", "error": True })
        except:
            pass

        DatabaseConnection.root.accounts[username] = newAccount
        DatabaseConnection.root.emails[email] = True
        transaction.commit()

        if (DEBUG): print("Account created")

        return dict({ "msg": "Account created", "error": False })
    
    def findAccountByUsername(self, username):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()

        DatabaseConnection.conn.sync()

        try: 
            account = DatabaseConnection.root.accounts[username]

            if (DEBUG): print("Account found")

            return dict({ "msg": "Account found", "result": account, "error": False })
        except: pass

        if (DEBUG): print("Account not found")

        return dict({ "msg": "Account not found", "error": True })
    
    def commitChanges(self, account):
        try: DatabaseConnection.root
        except: self.getDatabaseConnection()
        
        transaction.commit()

    def getDatabaseConnection(self):
        from modules import DatabaseConnection
        global DatabaseConnection

