from git import Repo
from pydriller import RepositoryMining

import inspect
import sys

DEBUG = True

class GitRepo():
    def __init__(self, repoLocation):
        self._repo = Repo(repoLocation)
        self._git = self._repo.git
        self._pydriller = Repo

    def _getPydriller(self, commitId):
        return RepositoryMining(self.dir(), commitId)

    def _addOption(self, option, text, addQuote=False, doubleDash=False):
        if (addQuote):
            dash = "-" if doubleDash == False else "--"

            return dash + option + " \"" + text + "\""
        else:
            dash = "-" if doubleDash == False else "--"

            return dash + option + " {}".format(text)
    
    def _printError(self):
        print("GitRepo Error: ", sys.exc_info()[0], sys.exc_info()[1])
    
    def add(self, fileName="."):
        try:
            self._git.add(fileName)
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stdout)[12:-1]})
    
    def unAdd(self, fileName=None):
        try:
            if (fileName == None):
                self._git.reset()
            else:
                self._git.reset(fileName)
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stdout)[12:-1]})
    
    def commit(self, text):
        try:
            self._git.commit(self._addOption("m", text, True))
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stdout)[12:-1]})
    
    def unCommit(self, deleteFiles=False):
        try:
            if (deleteFiles == True):
                self._git.reset(self._addOption("hard", "HEAD~1", False, True))
            else:
                self._git.reset("HEAD^")
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stdout)[12:-1]})
    
    def push(self, setUpstream = False):
        try:
            if setUpstream:
                self._git.push("--set-upstream", "origin", self.activeBranch())
            else:
                self._git.push()
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stderr)[12:-1]})
    
    def unPush(self):
        try:
            commitId = self._repo.head.commit.hexsha

            self._git.revert(commitId, no_edit = True)

            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stdout)[12:-1]})

    def pull(self):
        try:
            self._git.pull()
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stderr)[12:-1]})
    
    def checkout(self, branchName, newBranch=False):
        try:
            self._printError()

            if (newBranch):
                self._git.checkout("-b" + branchName)
            else:
                self._git.checkout(branchName)
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stderr)[12:-1]})
    
    def merge(self, branchName):
        try:
            self._git.merge(branchName)
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stderr)[12:-1]})

    def stash(self, pop = False):
        try:
            if pop:
                self._git.stash('pop')
            else:
                self._git.stash()
            return dict({ "error": False })
        except:
            if (DEBUG):
                self._printError()
            return dict({ "error": True, "msg": str(sys.exc_info()[1].stderr)[12:-1]})

    def branches(self, addStar = False):
        branches = []

        for branch in self._repo.branches:
            if (addStar):
                branches.append("* " + branch.name)
            else:
                branches.append(branch.name)

        return branches

    def activeBranch(self):
        return self._repo.active_branch.name
    
    def untrackedFiles(self):
        return [ item.a_path for item in self._repo.index.diff(None) ]
    
    def filterModifiedText(self, text):
        if "modified" in text or "new file" in text or "deleted" in text:
            return True
        return False

    def getUntrackedFilePaths(self):
        result = self._git.status().split('\n')
        modified = filter(self.filterModifiedText, result)
        filePaths = {}

        for text in modified:
            path = text[13:]
            pathList = path.split("/")
            fileName = pathList[-1]
            filePaths[fileName] = path

        return filePaths

    def fetch(self):
        self._git.fetch()
        self._printError()

    def dir(self):
        return self._repo._working_tree_dir
    
    def getFormattedCommits(self):
        commits = list(self._repo.iter_commits(self.activeBranch()))
        commitsDict = {}
        
        for commit in commits:
            commitsDict['{} --> {}'.format(commit.author, commit.message.replace("\n", ""))] = commit.hexsha
        
        return commitsDict
    
    def getModifications(self, commitId):
        for commit in self._getPydriller(commitId).traverse_commits():
            return commit.modifications
    def log_graph(self):
        return self._git.log(
            "--graph", 
            "--abbrev-commit", 
            "--decorate", 
            "--format=format:%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)",
            '--all'
        )
