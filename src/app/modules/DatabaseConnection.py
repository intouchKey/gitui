import ZODB.config
import psycopg2
import BTrees.OOBTree
import transaction

DEBUG = True
CLEAR_DATABASE = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': 'gitui.c5lkt7jzdgfq.us-east-2.rds.amazonaws.com',
        'PORT': '5432',
        'NAME': 'gitui',
        'USER': 'postgres',
        'PASSWORD': '2w3e4r5t',
    }
}

path = "config.zconfig"

def openConnection():
    db = ZODB.config.databaseFromURL(path)

    global conn
    global root

    conn = db.open()
    root = conn.root

    if(CLEAR_DATABASE):
        print("\n* CLEAR DATABASE *\n")
        del root.teams
        del root.accounts
        del root.emails
        del root.users

        transaction.commit()

    if(DEBUG): 
        try:
            root.accounts
        except:
            root.accounts = BTrees.OOBTree.BTree()

            transaction.commit()

            if(DEBUG): print("Successfully created accounts tree as a dict.")
        
        try:
            root.emails
        except:
            root.emails = BTrees.OOBTree.BTree()

            transaction.commit()

            if(DEBUG): print("Successfully created emails as a dict.")

        try:
            root.users
        except:
            root.users = BTrees.OOBTree.BTree()

            transaction.commit()

            if(DEBUG): print("Successfully created users tree as a dict.")
        
        try:
            root.teams
        except:
            root.teams = BTrees.OOBTree.BTree()

            transaction.commit()

            if(DEBUG): print("Successfully created teams tree as a dict.")

        print("Database:\n", root.__dict__, "\n")