class GitUISystem:
    def __init__(self):
        self.observers = []
    
    def addObserver(self, o):
        self.observers.append(o)

    def notifyObservers(self, data):
        for o in self.observers:
            o.updateAction(data)
    
    def updateLatestAction(self, action):
        self.notifyObservers(action)
