from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint, 
                            QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont, 
                            QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap, 
                            QRadialGradient)
from PySide2.QtWidgets import *

from entities.Repository import Repository

import git

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1101, 681)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.actionAdd = QAction(MainWindow)
        self.actionAdd.setObjectName(u"actionAdd")
        iconAdd = QIcon()
        iconAdd.addFile(u"src/app/images/icon/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAdd.setIcon(iconAdd)

        self.actionCommit = QAction(MainWindow)
        self.actionCommit.setObjectName(u"actionCommit")
        iconCommit = QIcon()
        iconCommit.addFile(u"src/app/images/icon/commit.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionCommit.setIcon(iconCommit)

        self.actionUndo = QAction(MainWindow)
        self.actionUndo.setObjectName(u"actionUndo")
        icon = QIcon()
        icon.addFile(u"src/app/images/icon/undo.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionUndo.setIcon(icon)
        self.actionPull = QAction(MainWindow)
        self.actionPull.setObjectName(u"actionPull")
        icon2 = QIcon()
        icon2.addFile(u"src/app/images/icon/pull.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPull.setIcon(icon2)
        self.actionPush = QAction(MainWindow)
        self.actionPush.setObjectName(u"actionPush")
        icon3 = QIcon()
        icon3.addFile(u"src/app/images/icon/push.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPush.setIcon(icon3)
        self.actionStash = QAction(MainWindow)
        self.actionStash.setObjectName(u"actionStash")
        icon4 = QIcon()
        icon4.addFile(u"src/app/images/icon/stash.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStash.setIcon(icon4)
        self.actionPop = QAction(MainWindow)
        self.actionPop.setObjectName(u"actionPop")
        icon5 = QIcon()
        icon5.addFile(u"src/app/images/icon/pop.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPop.setIcon(icon5)
        self.actionBranch = QAction(MainWindow)
        self.actionBranch.setObjectName(u"actionBranch")
        icon6 = QIcon()
        icon6.addFile(u"src/app/images/icon/branch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBranch.setIcon(icon6)
        self.actionUndo_2 = QAction(MainWindow)
        self.actionUndo_2.setObjectName(u"actionUndo_2")
        self.actionUndo_2.setIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.frame1 = QFrame(self.centralwidget)
        self.frame1.setObjectName(u"frame")
        self.frame1.setGeometry(QRect(-1, 0, 261, 641))
        self.frame1.setStyleSheet(u"background-image: url(src/app/images/sidebar.jpg);")
        self.frame1.setFrameShape(QFrame.StyledPanel)
        self.frame1.setFrameShadow(QFrame.Raised)
        self.userFullname = QLabel(self.centralwidget)
        self.userFullname.setObjectName(u"userFullname")
        self.userFullname.setGeometry(QRect(20, 200, 231, 25))
        font = QFont()
        font.setPointSize(20)
        self.userFullname.setFont(font)
        self.userFullname.setAlignment(Qt.AlignCenter)
        font1 = QFont()
        font1.setPointSize(22)
        self.verticalLine = QFrame(self.centralwidget)
        self.verticalLine.setObjectName(u"verticalLine")
        self.verticalLine.setGeometry(QRect(250, 0, 21, 631))
        self.verticalLine.setFrameShape(QFrame.VLine)
        self.verticalLine.setFrameShadow(QFrame.Sunken)
        self.horizontalLineSidebar = QFrame(self.centralwidget)
        self.horizontalLineSidebar.setObjectName(u"horizontalLineSidebar")
        self.horizontalLineSidebar.setGeometry(QRect(0, 240, 261, 16))
        self.horizontalLineSidebar.setFrameShape(QFrame.HLine)
        self.horizontalLineSidebar.setFrameShadow(QFrame.Sunken)
        self.gitUiPixmap = QPixmap("src/app/images/logo-small.png")
        self.userIcon = QLabel(self.centralwidget)
        self.userIcon.setObjectName(u"userIcon")
        self.userIcon.setGeometry(QRect(10, 92, 235, 80))
        self.userIcon.setPixmap(self.gitUiPixmap)
        self.userIcon.setScaledContents(1)
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(260, 0, 851, 641))
        self.frame.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(220, 110, 361, 181))
        self.label.setStyleSheet(u"image: url(src/app/images/logo-medium.png);")
        self.chooseRepoButton = QPushButton(self.frame)
        self.chooseRepoButton.setObjectName(u"pushButton")
        self.chooseRepoButton.setGeometry(QRect(310, 320, 181, 41))
        font2 = QFont()
        font2.setFamily(u"Yu Gothic UI Light")
        font2.setPointSize(13)
        font2.setBold(True)
        font2.setWeight(75)
        self.chooseRepoButton.setFont(font2)
        self.chooseRepoButton.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                        " border-style: solid;\n"
                                        " border-width:0px;\n"
                                        " border-radius:20px;\n"
                                        "color: white;")

        self.cloneRepositoryButton = QPushButton(self.frame)
        self.cloneRepositoryButton.setObjectName(u"pushButton")
        self.cloneRepositoryButton.setGeometry(QRect(310, 390, 181, 41))
        self.cloneRepositoryButton.setFont(font2)
        self.cloneRepositoryButton.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                        " border-style: solid;\n"
                                        " border-width:0px;\n"
                                        " border-radius:20px;\n"
                                        "color: white;")


        font3 = QFont()
        font3.setPointSize(16)

        icon8 = QIcon()
        icon8.addFile(u"src/app/images/teamManagement.png", QSize(), QIcon.Normal, QIcon.Off)

        icon9 = QIcon()
        icon9.addFile(u"src/app/images/settings.png", QSize(), QIcon.Normal, QIcon.Off)

        icon10 = QIcon()
        icon10.addFile(u"src/app/images/baseline_date_range_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)

        icon11 = QIcon()
        icon11.addFile(u"src/app/images/baseline_file_copy_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)

        self.teamButton = QPushButton(self.centralwidget)
        self.teamButton.setObjectName(u"teamButton")
        self.teamButton.setGeometry(QRect(10, 260, 241, 61))
        self.teamButton.setFont(font3)
        self.teamButton.setIcon(icon8)
        self.teamButton.setIconSize(QSize(32, 32))

        self.teamManagementButton = QPushButton(self.centralwidget)
        self.teamManagementButton.setObjectName(u"teamManagementButton")
        self.teamManagementButton.setGeometry(QRect(10, 330, 241, 61))
        self.teamManagementButton.setFont(font3)
        self.teamManagementButton.setIconSize(QSize(32, 32))
        self.teamManagementButton.setIcon(icon10)

        self.branchButton = QPushButton(self.centralwidget)
        self.branchButton.setObjectName(u"branchButton")
        self.branchButton.setGeometry(QRect(10, 400, 241, 61))
        self.branchButton.setFont(font3)
        self.branchButton.setIcon(icon6)
        self.branchButton.setIconSize(QSize(32, 32))

        self.compareButton = QPushButton("Compare files", self.centralwidget)
        self.compareButton.setFont(font3)
        self.compareButton.setObjectName(u"branchButton")
        self.compareButton.setGeometry(QRect(10, 470, 241, 61))
        self.compareButton.setIcon(icon11)
        self.compareButton.setIconSize(QSize(32, 32))

        self.settingsButton = QPushButton(self.centralwidget)
        self.settingsButton.setObjectName(u"settingsButton")
        self.settingsButton.setGeometry(QRect(10, 540, 241, 61))
        self.settingsButton.setFont(font3)
        self.settingsButton.setIcon(icon9)
        self.settingsButton.setIconSize(QSize(32, 32))

        MainWindow.setCentralWidget(self.centralwidget)
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setEnabled(True)
        self.toolBar.setLayoutDirection(Qt.LeftToRight)
        self.toolBar.setAutoFillBackground(False)
        self.toolBar.setStyleSheet(u"spacing: 10px;")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        leftSpacer = QWidget()
        leftSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        rightSpacer = QWidget()
        rightSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.toolBar.addWidget(leftSpacer)
        self.toolBar.addAction(self.actionUndo_2)
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionCommit)
        self.toolBar.addAction(self.actionPull)
        self.toolBar.addAction(self.actionPush)
        self.toolBar.addAction(self.actionBranch)
        self.toolBar.addAction(self.actionStash)
        self.toolBar.addAction(self.actionPop)
        self.toolBar.addWidget(rightSpacer)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Home", None))
        self.actionUndo.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionAdd.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.actionCommit.setText(QCoreApplication.translate("MainWindow", u"Commit", None))
        self.actionPull.setText(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPull.setToolTip(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPush.setText(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionPush.setToolTip(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionStash.setText(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionStash.setToolTip(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionPop.setText(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionPop.setToolTip(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionBranch.setText(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionBranch.setToolTip(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionUndo_2.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionUndo_2.setToolTip(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.branchButton.setText(QCoreApplication.translate("MainWindow", u"Branch Management", None))
        self.teamManagementButton.setText(QCoreApplication.translate("MainWindow", u"Team Management", None))
        self.userIcon.setText("")
        self.teamButton.setText(QCoreApplication.translate("MainWindow", u"Team board", None))
        self.settingsButton.setText(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.label.setText("")
        self.chooseRepoButton.setText(QCoreApplication.translate("MainWindow", u"Choose your repository", None))
        self.cloneRepositoryButton.setText(QCoreApplication.translate("MainWindow", u"Clone your repository", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))

class HomePage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.chooseRepoButton.clicked.connect(self.chooseRepo)
        self.ui.cloneRepositoryButton.clicked.connect(self.cloneRepository)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

    def updateAction(self, action):
        if self.isOpen:
            self.pushNotification("Action: (" + action + ") done successfully")

    def pushNotification(self, text):
        try:
            import pync

            pync.notify(text, title="GitUI")
        except:
            from win10toast import ToastNotifier

            toast = ToastNotifier()
            toast.show_toast("GitUI", text, duration=0)

    def chooseRepo(self, new = False):
        directoryPath = QFileDialog.getExistingDirectory(self)

        if len(directoryPath) == 0:
            return

        try:
            repository = Repository(directoryPath)

            success = self.data['user'].setRepository(repository)

            if not success:
                msgBox = QMessageBox(self)

                msgBox.setText("An error occured.")
                msgBox.show()

                return
            
            result = self.userController.updateUser(self.data['user'])
            
            if result['error']:
                msgBox = QMessageBox(self)

                msgBox.setText("An error occured.")
                msgBox.show()

                return 

            self.close()
            self.data["pages"]["TeamOpPage"].show()
        except Exception as e:
            print(e)
            self._showMessageBox("The directory you chose is not a repository.")
            
    def cloneRepository(self):
        link, ok = QInputDialog().getText(
            self, 
            "Clone repo dialog", 
            "Please enter the link here. After this, you will be prompt to choose the directory you want to clone your repo. \nPlease make sure you set up your username and password / SSH properly before using the git functions", 
            QLineEdit.Normal, 
            ""
        )

        if ok:
            directoryPath = QFileDialog.getExistingDirectory(self)

            if len(directoryPath) == 0:
                return

            try:
                git.Git(directoryPath).clone(link)
                self._showMessageBox("Clone successfully.")
            except Exception as e:
                print(e)
                self._showMessageBox("Incorrect link / Internet connection error.")

    def _showMessageBox(self, text):
        msgBox = QMessageBox(self, "GitUI message")

        msgBox.setText(text)
        msgBox.show()