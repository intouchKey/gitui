from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1100, 680)
        font2 = QFont()
        font2.setPointSize(15)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayoutWidget = QWidget(self.centralwidget)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(0, 0, 261, 691))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.verticalLayoutWidget)
        self.frame.setObjectName(u"frame")
        self.frame.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)

        self.verticalLayout.addWidget(self.frame)

        self.verticalLayoutWidget_2 = QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(260, 0, 841, 681))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_2 = QFrame(self.verticalLayoutWidget_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.logo = QLabel(self.frame_2)
        self.logo.setObjectName(u"logo")
        self.logo.setGeometry(QRect(180, 110, 461, 191))
        self.logo.setPixmap(QPixmap(u"src/app/images/logo-medium.png"))
        self.usernameEntry = QLineEdit(self.frame_2)
        self.usernameEntry.setObjectName(u"usernameEntry")
        self.usernameEntry.setPlaceholderText("Username")
        self.usernameEntry.setGeometry(QRect(230, 330, 371, 41))
        font = QFont()
        font.setFamily(u"Yu Gothic UI Light")
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.usernameEntry.setFont(font)
        self.usernameEntry.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.usernameEntry.setAlignment(Qt.AlignCenter)
        self.emailEntry = QLineEdit(self.frame_2)
        self.emailEntry.setObjectName(u"emailEntry")
        self.emailEntry.setPlaceholderText("Email")
        self.emailEntry.setGeometry(QRect(230, 390, 371, 41))
        self.emailEntry.setFont(font)
        self.emailEntry.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.emailEntry.setAlignment(Qt.AlignCenter)
        self.passwordEntry = QLineEdit(self.frame_2)
        self.passwordEntry.setObjectName(u"passwordEntry")
        self.passwordEntry.setPlaceholderText("Password")
        self.passwordEntry.setGeometry(QRect(230, 450, 371, 41))
        self.passwordEntry.setFont(font2)
        self.passwordEntry.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.passwordEntry.setAlignment(Qt.AlignCenter)
        self.passwordEntry.setEchoMode(QLineEdit.Password)
        self.conPasswordEntry = QLineEdit(self.frame_2)
        self.conPasswordEntry.setObjectName(u"conPasswordEntry")
        self.conPasswordEntry.setPlaceholderText("Confirm password")
        self.conPasswordEntry.setGeometry(QRect(230, 510, 191, 41))
        self.conPasswordEntry.setFont(font2)
        self.conPasswordEntry.setStyleSheet(u" background-color: white;\n"
                                                " border-style: solid;\n"
                                                " border-width:2px;\n"
                                                " border-radius:20px;\n"
                                                "border-color: rgb(25, 182, 255);")
        self.verifyEntry = QLineEdit(self.frame_2)
        self.verifyEntry.setObjectName(u"verifyEntry")
        self.verifyEntry.setGeometry(QRect(230, 570, 191, 41))
        self.verifyEntry.setPlaceholderText("Verification code")
        self.verifyEntry.setFont(font)
        self.verifyEntry.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.verifyEntry.setAlignment(Qt.AlignCenter)
        self.conPasswordEntry.setAlignment(Qt.AlignCenter)
        self.conPasswordEntry.setEchoMode(QLineEdit.Password)
        self.registerButton = QPushButton(self.frame_2)
        self.registerButton.setObjectName(u"registerButton")
        self.registerButton.setGeometry(QRect(440, 510, 151, 41))
        font1 = QFont()
        font1.setFamily(u"Yu Gothic UI Light")
        font1.setPointSize(13)
        font1.setBold(True)
        font1.setWeight(75)
        self.registerButton.setFont(font1)
        self.registerButton.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                            " border-style: solid;\n"
                                            " border-width:0px;\n"
                                            " border-radius:20px;\n"
                                            "color: white;")
        self.verifyButton = QPushButton(self.frame_2)
        self.verifyButton.setObjectName(u"verifyButton")
        self.verifyButton.setGeometry(QRect(440, 570, 151, 41))
        self.verifyButton.setFont(font1)
        self.verifyButton.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                            " border-style: solid;\n"
                                            " border-width:0px;\n"
                                            " border-radius:20px;\n"
                                            "color: white;")
        self.exitButton = QPushButton(self.frame_2)
        self.exitButton.setObjectName(u"exitButton")
        self.exitButton.setGeometry(QRect(20, 20, 31, 32))
        self.exitButton.setStyleSheet(u"image: url(src/app/images/icon/exit.png);")
        self.exitButton.setFlat(True)

        self.verticalLayout_2.addWidget(self.frame_2)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Register", None))
        self.logo.setText("")
        self.usernameEntry.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.passwordEntry.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.registerButton.setText(QCoreApplication.translate("MainWindow", u"REGISTER", None))
        self.conPasswordEntry.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.verifyEntry.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.verifyButton.setText(QCoreApplication.translate("MainWindow", u"Verify Code", None))
        self.emailEntry.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.exitButton.setText("")

class RegisterPage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

        self.ui.exitButton.clicked.connect(self.exitToLogin)
        self.ui.registerButton.clicked.connect(self.register)
        self.ui.verifyButton.clicked.connect(self.verify)
        self.ui.registerButton.setEnabled(True)
        self.ui.verifyButton.setEnabled(False)

    def exitToLogin(self):
        self.close()
        self.data["pages"]["LoginPage"].show()

    def register(self):
        print("register")
        username = self.ui.usernameEntry.text()
        email = self.ui.emailEntry.text()
        password = self.ui.passwordEntry.text()
        conPassword = self.ui.conPasswordEntry.text()

        if username == "":
            msgBox = QMessageBox(self)

            msgBox.setText('Username cannot be empty')
            msgBox.show()

            return
        elif email == "":
            msgBox = QMessageBox(self)

            msgBox.setText('Email cannot be empty')
            msgBox.show()

            return
        elif password == "":
            msgBox = QMessageBox(self)

            msgBox.setText('Password cannot be empty')
            msgBox.show()

            return
        elif not(password == conPassword):
            msgBox = QMessageBox(self)

            msgBox.setText('Incorrect password confirmation be empty')
            msgBox.show()

            return

        result = self.accountController.createAccount(username, password, email)
        result2 = self.userController.createUser(username, email)

        if (result['error'] == False and result2['error'] == False):
            self.ui.usernameEntry.setDisabled(1)
            self.ui.emailEntry.setDisabled(1)
            self.ui.passwordEntry.setDisabled(1)
            self.ui.conPasswordEntry.setDisabled(1)
            self.ui.registerButton.setEnabled(False)
            self.ui.verifyButton.setEnabled(True)

            msgBox = QMessageBox(self)

            msgBox.setText('Verification code has been sent to your email')
            msgBox.show()

            return

        msgBox = QMessageBox(self)

        msgBox.setText(result['msg'])
        msgBox.show()

    def verify(self):
        username = self.ui.usernameEntry.text()
        verificationCode = self.ui.verifyEntry.text()

        result = self.accountController.verifyAccount(username, verificationCode)

        if (result['error'] == False):
            msgBox = QMessageBox(self)

            msgBox.setText(result['msg'])
            msgBox.show()

            return

        msgBox = QMessageBox(self)

        msgBox.setText(result['msg'])
        msgBox.show()

