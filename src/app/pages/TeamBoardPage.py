from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

class TeamBoardPage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.listener = None
        self.firstTimeOpen = True
        self.windowClose = False
        self.notificationEvent = False

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

    def updateAction(self, action):
        if self.isOpen:
            self.updateHistoryForDashboard()
            self.pushNotification("Action: (" + action + ") done successfully")

    def pushNotification(self, text):
        try:
            import pync

            pync.notify(text, title="GitUI")
        except:
            from win10toast import ToastNotifier

            toast = ToastNotifier()
            toast.show_toast("GitUI", text, duration=0)

    def updateHistoryForDashboard(self):
        teamName = self.data['user'].getTeamName()

        if (teamName == None):
            return

        data = self.teamController.teamRepository.findTeamByTeamName(teamName)
        team = data['result']

        dashboardData = dict()

        for member in team.getUsers():
            latestHistory = member.getLatestHistory()
            
            if (not latestHistory):
                time = "No data"
                files = []
                operationName = "None"
            else:
                time = latestHistory.getModifiedTime()
                files = latestHistory.getPathOfFilesModified()
                operationName = latestHistory.getOperationName()

            dashboardData[member.getUsername()] = {
                                                    "lastModified": time,
                                                    "path": files,
                                                    "operationName": operationName
                                                    }

        notificationArea = self.ui.notificationArea

        for i in reversed(range(notificationArea.verticalLayout.count())):
            notificationArea.verticalLayout.itemAt(i).widget().setParent(None)

        notificationArea.bar = QLabel(notificationArea.scrollAreaWidgetContent)
        notificationArea.bar.setStyleSheet("background-color: #3c78d8")
        notificationArea.bar.setMinimumHeight(20)
        notificationArea.bar.setGeometry(QRect(0, 0, 839, 639))
        notificationArea.verticalLayout.addWidget(notificationArea.bar)
        notificationArea.verticalLayout.setAlignment(notificationArea.bar, Qt.AlignTop) 

        i = 0
        for key, value in dashboardData.items():  
            notification = QLabel(notificationArea.scrollAreaWidgetContent)
            form = Ui_Form(developerName = key,
                           lastModifiedTime = value['lastModified'],
                           fileNameModified = value['path'],
                           operationName = value['operationName'])
            form.setupUi(notification)
            notification.setMinimumSize(839, 200)
            notification.setAlignment(Qt.AlignTop)
            styleSheet = "border: 0px;background-color: " + notificationArea.colors[i%4]
            notification.setStyleSheet(styleSheet)
            
            notificationArea.verticalLayout.addWidget(notification)
            notificationArea.verticalLayout.setAlignment(notification, Qt.AlignTop)
            
            i += 1

class Ui_Form(object):
    def __init__(self, developerName = None,
                 lastModifiedTime = None,
                 fileNameModified = None,
                 operationName = None):

        self.developerName = developerName
        self.lastModifiedTime = lastModifiedTime
        self.fileNameModified = fileNameModified
        self.operationName = operationName

        self.iconPixmap = QPixmap('src/app/images/baseline_dashboard_white_18dp.png')
        self.addIcon = QPixmap('src/app/images/icon/addWhite.png')
        self.commitIcon = QPixmap('src/app/images/icon/commitWhite.png')
        self.pushIcon = QPixmap('src/app/images/pushWhite.png')

    def setupUi(self, Form):
        if Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(839, 200)
        self.frame = QFrame(Form)
        self.frame.resize(839, 200)
        
        self.icon = QLabel(Form)
        self.icon.setObjectName(u"icon")
        self.icon.setGeometry(QRect(35, 35, 45, 45))
        self.icon.setAlignment(Qt.AlignCenter)
        self.icon.setPixmap(self.iconPixmap)

        font1 = QFont()
        font1.setPointSize(22)
        font2 = QFont()
        font2.setPointSize(16)
        self.usernameLabel = QLabel(Form)
        self.usernameLabel.setText(self.developerName)
        self.usernameLabel.setStyleSheet("color: #AEAEAE")
        self.usernameLabel.setObjectName(u"usernameLabel")
        self.usernameLabel.setGeometry(QRect(100, 20, 600, 31))

        self.lastEditLabel = QLabel(Form)
        self.lastEditLabel.setObjectName(u"lastEditLabel")
        self.lastEditLabel.setStyleSheet("color: #FFFFFF")
        self.lastEditLabel.setGeometry(QRect(100, 60, 600, 20))
        self.lastEditLabel.setText("Last edit: " + self.lastModifiedTime)

        self.stateLabel = QLabel(Form)
        self.stateLabel.setObjectName(u"icon")
        self.stateLabel.setGeometry(QRect(720, 20, 45, 45))
        self.stateLabel.setAlignment(Qt.AlignCenter)

        if self.operationName == "Add":
            self.stateLabel.setPixmap(self.addIcon)
        elif self.operationName == "Commit":
            self.stateLabel.setPixmap(self.commitIcon)
        elif self.operationName == "Push":
            self.stateLabel.setPixmap(self.pushIcon)

        self.usernameLabel.setFont(font1)
        self.lastEditLabel.setFont(font2)

        self.pathList = QListWidget(Form)
        self.pathList.setGeometry(QRect(100, 90, 719, 100))

        for i in range(len(self.fileNameModified)):
            self.pathList.addItem("- " + self.fileNameModified[i])
            self.pathList.item(i).setFont(font2)
            self.pathList.item(i).setTextColor(QColor(255, 255, 255))

        Form.lower()

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))

class NotificationArea(QWidget):
    def __init__(self, parent = None, numberOfNotifications = None):
        self.colors = ["#1c3464", "#034569", "#235b79", "#086ca2", "#3c9dd0"]
        QWidget.__init__(self, parent)
        self.resize(841, 641)
        
        self.scrollAreaWidgetContent = QWidget()
        self.scrollAreaWidgetContent.setGeometry(QRect(0, 0, 841, 639))

        self.verticalLayout = QVBoxLayout()
        
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setAlignment(Qt.AlignTop)
        
        self.bar = QLabel(self.scrollAreaWidgetContent)
        self.bar.setStyleSheet("background-color: #3c78d8")
        self.bar.setMinimumHeight(20)
        self.bar.setGeometry(QRect(0, 0, 839, 639))

        self.verticalLayout.addWidget(self.bar)
        self.verticalLayout.setAlignment(self.bar, Qt.AlignTop)   

        self.scrollAreaWidgetContent.setLayout(self.verticalLayout)
        
        self.scrollAreaWidgetContent.setLayout(self.verticalLayout)

        self.scrollArea = QScrollArea()
        self.scrollArea.setGeometry(QRect(0, 0, 841, 641))
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setContentsMargins(0, 0, 0, 0)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollAreaWidgetContent)
        
        layout = QVBoxLayout(self)
        layout.addWidget(self.scrollArea)
        layout.setContentsMargins(0,0,0,0)

class Ui_MainWindow(object):
    def __init__(self):
        self.numberOfNotifications = 5
    
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1101, 681)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.actionAdd = QAction(MainWindow)
        self.actionAdd.setObjectName(u"actionAdd")
        iconAdd = QIcon()
        iconAdd.addFile(u"src/app/images/icon/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAdd.setIcon(iconAdd)

        self.actionCommit = QAction(MainWindow)
        self.actionCommit.setObjectName(u"actionCommit")
        iconCommit = QIcon()
        iconCommit.addFile(u"src/app/images/icon/commit.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionCommit.setIcon(iconCommit)

        self.actionUndo = QAction(MainWindow)
        self.actionUndo.setObjectName(u"actionUndo")
        icon = QIcon()
        icon.addFile(u"src/app/images/icon/undo.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionUndo.setIcon(icon)
        self.actionPull = QAction(MainWindow)
        self.actionPull.setObjectName(u"actionPull")
        icon2 = QIcon()
        icon2.addFile(u"src/app/images/icon/pull.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPull.setIcon(icon2)
        self.actionPush = QAction(MainWindow)
        self.actionPush.setObjectName(u"actionPush")
        icon3 = QIcon()
        icon3.addFile(u"src/app/images/icon/push.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPush.setIcon(icon3)
        self.actionStash = QAction(MainWindow)
        self.actionStash.setObjectName(u"actionStash")
        icon4 = QIcon()
        icon4.addFile(u"src/app/images/icon/stash.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStash.setIcon(icon4)
        self.actionPop = QAction(MainWindow)
        self.actionPop.setObjectName(u"actionPop")
        icon5 = QIcon()
        icon5.addFile(u"src/app/images/icon/pop.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPop.setIcon(icon5)
        self.actionBranch = QAction(MainWindow)
        self.actionBranch.setObjectName(u"actionBranch")
        icon6 = QIcon()
        icon6.addFile(u"src/app/images/icon/branch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBranch.setIcon(icon6)
        self.actionUndo_2 = QAction(MainWindow)
        self.actionUndo_2.setObjectName(u"actionUndo_2")
        self.actionUndo_2.setIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.branchButton = QPushButton(self.centralwidget)
        self.branchButton.setObjectName(u"branchButton")
        self.branchButton.setGeometry(QRect(10, 400, 241, 61))
        font = QFont()
        font.setPointSize(16)
        self.branchButton.setFont(font)
        self.branchButton.setIcon(icon6)
        self.branchButton.setIconSize(QSize(32, 32))

        icon11 = QIcon()
        icon11.addFile(u"src/app/images/baseline_file_copy_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)
        
        self.compareButton = QPushButton("Compare files", self.centralwidget)
        self.compareButton.setFont(font)
        self.compareButton.setObjectName(u"branchButton")
        self.compareButton.setGeometry(QRect(10, 470, 241, 61))
        self.compareButton.setIcon(icon11)
        self.compareButton.setIconSize(QSize(32, 32))

        self.verticalLine = QFrame(self.centralwidget)
        self.verticalLine.setObjectName(u"verticalLine")
        self.verticalLine.setGeometry(QRect(250, 0, 21, 631))
        self.verticalLine.setFrameShape(QFrame.VLine)
        self.verticalLine.setFrameShadow(QFrame.Sunken)
        self.horizontalLineSidebar = QFrame(self.centralwidget)
        self.horizontalLineSidebar.setObjectName(u"horizontalLineSidebar")
        self.horizontalLineSidebar.setGeometry(QRect(0, 240, 261, 16))
        self.horizontalLineSidebar.setFrameShape(QFrame.HLine)
        self.horizontalLineSidebar.setFrameShadow(QFrame.Sunken)
        icon10 = QIcon()
        icon10.addFile(u"src/app/images/baseline_date_range_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)
        self.teamManagementButton = QPushButton(self.centralwidget)
        self.teamManagementButton.setObjectName(u"teamManagementButton")
        self.teamManagementButton.setGeometry(QRect(10, 330, 241, 61))
        self.teamManagementButton.setFont(font)
        self.teamManagementButton.setIconSize(QSize(32, 32))
        self.teamManagementButton.setIcon(icon10)
        self.teamButton = QPushButton(self.centralwidget)
        self.teamButton.setObjectName(u"teamButton")
        self.teamButton.setGeometry(QRect(10, 260, 241, 61))
        self.teamButton.setFont(font)
        icon8 = QIcon()
        icon8.addFile(u"src/app/images/teamManagement.png", QSize(), QIcon.Normal, QIcon.Off)
        self.teamButton.setIcon(icon8)
        self.teamButton.setIconSize(QSize(32, 32))
        self.settingsButton = QPushButton(self.centralwidget)
        self.settingsButton.setObjectName(u"settingsButton")
        self.settingsButton.setGeometry(QRect(10, 540, 241, 61))
        self.settingsButton.setFont(font)
        icon9 = QIcon()
        icon9.addFile(u"src/app/images/settings.png", QSize(), QIcon.Normal, QIcon.Off)
        self.settingsButton.setIcon(icon9)
        self.settingsButton.setIconSize(QSize(32, 32))
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(-1, 0, 261, 641))
        self.frame.setStyleSheet(u"background-image: url(src/app/images/sidebar.jpg);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gitUiPixmap = QPixmap("src/app/images/logo-small.png")
        self.userIcon = QLabel(self.centralwidget)
        self.userIcon.setObjectName(u"userIcon")
        self.userIcon.setGeometry(QRect(10, 92, 235, 80))
        self.userIcon.setPixmap(self.gitUiPixmap)
        self.userIcon.setScaledContents(1)
        self.userFullname = QLabel(self.centralwidget)
        self.userFullname.setObjectName(u"userFullname")
        self.userFullname.setGeometry(QRect(20, 210, 221, 20))
        font1 = QFont()
        font1.setPointSize(20)
        self.userFullname.setFont(font1)
        self.userFullname.setAlignment(Qt.AlignCenter)

        self.frame2 = QFrame(self.centralwidget)
        self.frame2.setGeometry(QRect(260, 0, 841, 641))
        self.notificationArea = NotificationArea(self.frame2, self.numberOfNotifications)
        
        MainWindow.setCentralWidget(self.centralwidget)

        self.frame.raise_()
        self.branchButton.raise_()
        self.verticalLine.raise_()
        self.horizontalLineSidebar.raise_()
        self.teamManagementButton.raise_()
        self.userIcon.raise_()
        self.teamButton.raise_()
        self.settingsButton.raise_()
        self.compareButton.raise_()
        self.userFullname.raise_()
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setEnabled(True)
        self.toolBar.setLayoutDirection(Qt.LeftToRight)
        self.toolBar.setAutoFillBackground(False)
        self.toolBar.setStyleSheet(u"spacing: 10px;")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        leftSpacer = QWidget()
        leftSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        rightSpacer = QWidget()
        rightSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.toolBar.addWidget(leftSpacer)
        self.toolBar.addAction(self.actionUndo_2)
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionCommit)
        self.toolBar.addAction(self.actionPull)
        self.toolBar.addAction(self.actionPush)
        self.toolBar.addAction(self.actionBranch)
        self.toolBar.addAction(self.actionStash)
        self.toolBar.addAction(self.actionPop)
        self.toolBar.addWidget(rightSpacer)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Team Board", None))
        self.actionUndo.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionAdd.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.actionCommit.setText(QCoreApplication.translate("MainWindow", u"Commit", None))
        self.actionPull.setText(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPull.setToolTip(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPush.setText(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionPush.setToolTip(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionStash.setText(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionStash.setToolTip(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionPop.setText(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionPop.setToolTip(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionBranch.setText(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionBranch.setToolTip(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionUndo_2.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionUndo_2.setToolTip(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.branchButton.setText(QCoreApplication.translate("MainWindow", u"Branch Management", None))
        self.teamManagementButton.setText(QCoreApplication.translate("MainWindow", u"Team Management", None))
        self.userIcon.setText("")
        self.teamButton.setText(QCoreApplication.translate("MainWindow", u"Team board", None))
        self.settingsButton.setText(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))