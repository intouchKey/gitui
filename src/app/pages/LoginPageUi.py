from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1100, 680)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayoutWidget = QWidget(self.centralwidget)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(0, 0, 261, 691))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.verticalLayoutWidget)
        self.frame.setObjectName(u"frame")
        self.frame.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)

        self.verticalLayout.addWidget(self.frame)

        self.verticalLayoutWidget_2 = QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(260, 0, 841, 681))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_2 = QFrame(self.verticalLayoutWidget_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.logo = QLabel(self.frame_2)
        self.logo.setObjectName(u"logo")
        self.logo.setGeometry(QRect(180, 110, 461, 191))
        self.logo.setPixmap(QPixmap(u"src/app/images/logo-medium.png"))
        self.lineEdit = QLineEdit(self.frame_2)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(230, 330, 371, 41))
        self.lineEdit.setPlaceholderText("Username")
        font = QFont()
        font.setFamily(u"Yu Gothic UI Light")
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        font2 = QFont()
        font2.setPointSize(15)
        self.lineEdit.setFont(font)
        self.lineEdit.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.lineEdit.setAlignment(Qt.AlignCenter)
        self.lineEdit_2 = QLineEdit(self.frame_2)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setPlaceholderText("Password")
        self.lineEdit_2.setGeometry(QRect(230, 390, 371, 41))
        self.lineEdit_2.setFont(font2)
        self.lineEdit_2.setStyleSheet(u" background-color: white;\n"
                                            " border-style: solid;\n"
                                            " border-width:2px;\n"
                                            " border-radius:20px;\n"
                                            "border-color: rgb(25, 182, 255);")
        self.lineEdit_2.setAlignment(Qt.AlignCenter)
        self.lineEdit_2.setEchoMode(QLineEdit.Password)
        self.pushButton = QPushButton(self.frame_2)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(230, 470, 181, 41))
        font1 = QFont()
        font1.setFamily(u"Yu Gothic UI Light")
        font1.setPointSize(13)
        font1.setBold(True)
        font1.setWeight(75)
        self.pushButton.setFont(font1)
        self.pushButton.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                            " border-style: solid;\n"
                                            " border-width:0px;\n"
                                            " border-radius:20px;\n"
                                            "color: white;")
        self.pushButton_2 = QPushButton(self.frame_2)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(430, 470, 171, 41))
        self.pushButton_2.setFont(font1)
        self.pushButton_2.setStyleSheet(u"border-image: url(src/app/images/sidebar.jpg) 0 0 0 0 stretch stretch;\n"
                                            " border-style: solid;\n"
                                            " border-width:0px;\n"
                                            " border-radius:20px;\n"
                                            "color: white;")

        self.verticalLayout_2.addWidget(self.frame_2)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI", None))
        self.logo.setText("")
        self.lineEdit.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.lineEdit_2.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"LOGIN", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"REGISTER", None))

