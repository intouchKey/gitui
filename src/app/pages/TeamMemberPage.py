from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
                            QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
                           QRadialGradient)
from PySide2.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1101, 681)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.actionUndo = QAction(MainWindow)
        self.actionUndo.setObjectName(u"actionUndo")
        icon = QIcon()
        icon.addFile(u"src/app/images/icon/undo.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionUndo.setIcon(icon)
        self.actionPull = QAction(MainWindow)
        self.actionPull.setObjectName(u"actionPull")
        icon2 = QIcon()
        icon2.addFile(u"src/app/images/icon/pull.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPull.setIcon(icon2)
        self.actionPush = QAction(MainWindow)
        self.actionPush.setObjectName(u"actionPush")
        icon3 = QIcon()
        icon3.addFile(u"src/app/images/icon/push.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPush.setIcon(icon3)
        self.actionStash = QAction(MainWindow)
        self.actionStash.setObjectName(u"actionStash")
        icon4 = QIcon()
        icon4.addFile(u"src/app/images/icon/stash.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStash.setIcon(icon4)
        self.actionPop = QAction(MainWindow)
        self.actionPop.setObjectName(u"actionPop")
        icon5 = QIcon()
        icon5.addFile(u"src/app/images/icon/pop.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPop.setIcon(icon5)
        self.actionBranch = QAction(MainWindow)
        self.actionBranch.setObjectName(u"actionBranch")
        icon6 = QIcon()
        icon6.addFile(u"src/app/images/icon/branch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBranch.setIcon(icon6)
        self.actionUndo_2 = QAction(MainWindow)
        self.actionUndo_2.setObjectName(u"actionUndo_2")
        self.actionUndo_2.setIcon(icon)

        self.actionCommit = QAction(MainWindow)
        self.actionCommit.setObjectName(u"actionCommit")
        iconCommit = QIcon()
        iconCommit.addFile(u"src/app/images/icon/commit.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionCommit.setIcon(iconCommit)

        self.actionAdd = QAction(MainWindow)
        self.actionAdd.setObjectName(u"actionAdd")
        iconAdd = QIcon()
        iconAdd.addFile(u"src/app/images/icon/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAdd.setIcon(iconAdd)

        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")

        font = QFont()
        font.setPointSize(16)

        self.verticalLine = QFrame(self.centralwidget)
        self.verticalLine.setObjectName(u"verticalLine")
        self.verticalLine.setGeometry(QRect(250, 0, 21, 631))
        self.verticalLine.setFrameShape(QFrame.VLine)
        self.verticalLine.setFrameShadow(QFrame.Sunken)
        self.horizontalLineSidebar = QFrame(self.centralwidget)
        self.horizontalLineSidebar.setObjectName(u"horizontalLineSidebar")
        self.horizontalLineSidebar.setGeometry(QRect(0, 240, 261, 16))
        self.horizontalLineSidebar.setFrameShape(QFrame.HLine)
        self.horizontalLineSidebar.setFrameShadow(QFrame.Sunken)

        self.gitUiPixmap = QPixmap("src/app/images/logo-small.png")
        self.userIcon = QLabel(self.centralwidget)
        self.userIcon.setObjectName(u"userIcon")
        self.userIcon.setGeometry(QRect(10, 92, 235, 80))
        self.userIcon.setPixmap(self.gitUiPixmap)
        self.userIcon.setScaledContents(1)

        icon8 = QIcon()
        icon8.addFile(u"src/app/images/teamManagement.png", QSize(), QIcon.Normal, QIcon.Off)

        icon9 = QIcon()
        icon9.addFile(u"src/app/images/settings.png", QSize(), QIcon.Normal, QIcon.Off)

        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(-1, 0, 261, 641))
        self.frame.setStyleSheet(u"background-image: url(src/app/images/sidebar.jpg);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)

        self.frame2 = QFrame(self.centralwidget)
        self.frame2.setGeometry(QRect(260, 0, 841, 641))
        self.frame2.setStyleSheet("background-color: #CCDDFF")

        self.userFullname = QLabel(self.centralwidget)
        self.userFullname.setObjectName(u"userFullname")
        self.userFullname.setGeometry(QRect(20, 210, 221, 20))
        font1 = QFont()
        font1.setPointSize(20)
        self.userFullname.setFont(font1)
        self.userFullname.setAlignment(Qt.AlignCenter)

        self.membersList = QComboBox(self.centralwidget)
        self.membersList.setObjectName(u"membersList")
        self.membersList.setGeometry(QRect(290, 150, 381, 31))

        self.branchLabel = QLabel(self.centralwidget)
        self.branchLabel.setObjectName(u"label")
        self.branchLabel.setGeometry(QRect(310, 30, 81, 71))
        self.branchLabel.setPixmap(QPixmap(u"src/app/images/user.jpg"))
        self.branchLabel.setScaledContents(True)

        self.allMemberLabel = QLabel(self.centralwidget)
        self.allMemberLabel.setObjectName(u"allMemberLabel")
        self.allMemberLabel.setGeometry(QRect(300, 120, 121, 16))
        font2 = QFont()
        font2.setPointSize(14)
        self.allMemberLabel.setFont(font2)
        self.toBranchLabel = QLabel(self.centralwidget)
        self.toBranchLabel.setObjectName(u"toBranchLabel")
        self.toBranchLabel.setGeometry(QRect(710, 120, 101, 16))
        self.toBranchLabel.setFont(font2)
        self.teamMemberLabel = QLabel(self.centralwidget)
        self.teamMemberLabel.setObjectName(u"teamMemberLabel")
        self.teamMemberLabel.setGeometry(QRect(430, 50, 301, 41))
        font3 = QFont()
        font3.setPointSize(24)
        self.teamMemberLabel.setFont(font3)

        icon10 = QIcon()
        icon10.addFile(u"src/app/images/baseline_date_range_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)

        icon11 = QIcon()
        icon11.addFile(u"src/app/images/baseline_file_copy_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)

        self.teamButton = QPushButton(self.centralwidget)
        self.teamButton.setObjectName(u"teamButton")
        self.teamButton.setGeometry(QRect(10, 260, 241, 61))
        self.teamButton.setFont(font)
        self.teamButton.setIcon(icon8)
        self.teamButton.setIconSize(QSize(32, 32))

        self.teamManagementButton = QPushButton(self.centralwidget)
        self.teamManagementButton.setObjectName(u"teamManagmentButton")
        self.teamManagementButton.setGeometry(QRect(10, 330, 241, 61))
        self.teamManagementButton.setFont(font)
        self.teamManagementButton.setIconSize(QSize(32, 32))
        self.teamManagementButton.setIcon(icon10)

        self.branchButton = QPushButton(self.centralwidget)
        self.branchButton.setObjectName(u"branchButton")
        self.branchButton.setGeometry(QRect(10, 400, 241, 61))
        self.branchButton.setFont(font)
        self.branchButton.setIcon(icon6)
        self.branchButton.setIconSize(QSize(32, 32))

        self.settingsButton = QPushButton(self.centralwidget)
        self.settingsButton.setObjectName(u"settingsButton")
        self.settingsButton.setGeometry(QRect(10, 540, 241, 61))
        self.settingsButton.setFont(font)
        self.settingsButton.setIcon(icon9)
        self.settingsButton.setIconSize(QSize(32, 32))

        self.compareButton = QPushButton(self.centralwidget)
        self.compareButton.setObjectName(u"compareButton")
        self.compareButton.setGeometry(QRect(10, 470, 241, 61))
        self.compareButton.setFont(font)
        self.compareButton.setIcon(icon11)
        self.compareButton.setIconSize(QSize(32, 32))

        self.removeMemberButton = QPushButton(self.centralwidget)
        self.removeMemberButton.setObjectName(u"removeMemberButton")
        self.removeMemberButton.setGeometry(QRect(550, 210, 281, 51))
        self.removeMemberButton.setStyleSheet("background-color: #40a9ff; color: white")
        self.removeMemberButton.setFont(font2)

        MainWindow.setCentralWidget(self.centralwidget)
        self.frame.raise_()
        self.userFullname.raise_()
        self.branchButton.raise_()
        self.verticalLine.raise_()
        self.horizontalLineSidebar.raise_()
        self.teamManagementButton.raise_()
        self.userIcon.raise_()
        self.teamButton.raise_()
        self.settingsButton.raise_()
        self.membersList.raise_()
        self.branchLabel.raise_()
        self.allMemberLabel.raise_()
        self.toBranchLabel.raise_()
        self.teamMemberLabel.raise_()
        self.removeMemberButton.raise_()
        self.compareButton.raise_()
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setEnabled(True)
        self.toolBar.setLayoutDirection(Qt.LeftToRight)
        self.toolBar.setAutoFillBackground(False)
        self.toolBar.setStyleSheet(u"spacing: 10px;")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        leftSpacer = QWidget()
        leftSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        rightSpacer = QWidget()
        rightSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.toolBar.addWidget(leftSpacer)
        self.toolBar.addAction(self.actionUndo_2)
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionCommit)
        self.toolBar.addAction(self.actionPull)
        self.toolBar.addAction(self.actionPush)
        self.toolBar.addAction(self.actionBranch)
        self.toolBar.addAction(self.actionStash)
        self.toolBar.addAction(self.actionPop)
        self.toolBar.addWidget(rightSpacer)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Branch Management", None))
        self.actionUndo.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionAdd.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.actionCommit.setText(QCoreApplication.translate("MainWindow", u"Commit", None))
        self.actionPull.setText(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPull.setToolTip(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPush.setText(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionPush.setToolTip(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionStash.setText(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionStash.setToolTip(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionPop.setText(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionPop.setToolTip(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionBranch.setText(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionBranch.setToolTip(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionUndo_2.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionUndo_2.setToolTip(QCoreApplication.translate("MainWindow", u"Undo", None))

        self.branchButton.setText(QCoreApplication.translate("MainWindow", u"Branch Management", None))
        self.teamManagementButton.setText(QCoreApplication.translate("MainWindow", u"Team Management", None))
        self.userIcon.setText("")
        self.teamButton.setText(QCoreApplication.translate("MainWindow", u"Team board", None))
        self.settingsButton.setText(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.userFullname.setText(QCoreApplication.translate("MainWindow", u"Pokin Chaitanasakul", None))
        self.branchLabel.setText("")
        self.allMemberLabel.setText(QCoreApplication.translate("MainWindow", u"All Members", None))
        self.teamMemberLabel.setText(QCoreApplication.translate("MainWindow", u"Team Members", None))
        self.removeMemberButton.setText(QCoreApplication.translate("MainWindow", u"Remove Member", None))
        self.compareButton.setText(QCoreApplication.translate("MainWindow", u"Compare files", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))

class TeamMemberPage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

        self.ui.membersList.currentIndexChanged.connect(self.selectedMember)
        self.ui.removeMemberButton.clicked.connect(self.onClickRemoveMemberButton)

    def updateAction(self, action):
        if self.isOpen:
            self.pushNotification("Action: (" + action + ") done successfully")

    def pushNotification(self, text):
        try:
            import pync

            pync.notify(text, title="GitUI")
        except:
            from win10toast import ToastNotifier

            toast = ToastNotifier()
            toast.show_toast("GitUI", text, duration=0)

    def updateTeamMember(self):
        teamName = self.data['user'].getTeamName()

        if (teamName == None):
            return

        data = self.teamController.teamRepository.findTeamByTeamName(teamName)
        team = data['result']

        self.ui.membersList.clear()

        for member in team.getUsers():
            if not member.getIsLeader():
                self.ui.membersList.addItem(member.getUsername())

    def selectedMember(self):
        return self.ui.membersList.currentText()

    def countMembers(self):
        return self.ui.membersList.count()
        
    def onClickRemoveMemberButton(self):
        numberOfMembers = self.countMembers()

        if numberOfMembers == 0:
            msgBox = QMessageBox(self)  
            msgBox.setText("No members in your team!")
            msgBox.show()
            return
        
        result = self.userController.userRepository.findUserByUsername(self.selectedMember())
        if result['error']:
            msgBox = QMessageBox(self)
            msgBox.setText(result['msg'])
            msgBox.show()
            return

        userToBeRemoved = result['user']
        teamName = self.data['user'].getTeamName()

        result = self.teamController.leaveTeam(teamName, userToBeRemoved, self.userController.userRepository)

        if not result['error']:
            msgBox = QMessageBox(self)
            msgBox.setText("Remove member successfully")
            msgBox.show()
            self.updateTeamMember()
            return
        
        msgBox = QMessageBox(self)
        msgBox.setText("An error occured")
        msgBox.show()
