from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

class LogGraphArea(QWidget):
    def __init__(self, parent = None, numberOfNotifications = None):
        QWidget.__init__(self, parent)
        self.resize(841, 641)
        
        self.scrollAreaWidgetContent = QWidget()
        self.scrollAreaWidgetContent.setStyleSheet("background-color: #021A4B")
        self.scrollAreaWidgetContent.setGeometry(QRect(0, 0, 841, 639))

        self.verticalLayout = QVBoxLayout()
        
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setAlignment(Qt.AlignTop)
        
        self.bar = QLabel(self.scrollAreaWidgetContent)
        self.bar.setStyleSheet("background-color: #3c78d8")
        self.bar.setMinimumHeight(20)
        self.bar.setGeometry(QRect(0, 0, 839, 639))

        self.verticalLayout.addWidget(self.bar)
        self.verticalLayout.setAlignment(self.bar, Qt.AlignTop) 

        self.scrollAreaWidgetContent.setLayout(self.verticalLayout)


        self.scrollArea = QScrollArea()
        self.scrollArea.setGeometry(QRect(0, 0, 841, 641))
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setContentsMargins(0, 0, 0, 0)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollAreaWidgetContent)
        
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.scrollArea)
        self.layout.setContentsMargins(0,0,0,0)

    def setLogGraph(self, label):
        font = QFont()
        font.setPointSize(12)

        label.setParent(self.scrollAreaWidgetContent)
        label.setContentsMargins(10, 10, 0, 10)
        label.setFont(font)

        self.verticalLayout.addWidget(label)
        self.verticalLayout.setAlignment(label, Qt.AlignTop)

    def addBar(self):
        self.verticalLayout.addWidget(self.bar)
        self.verticalLayout.setAlignment(self.bar, Qt.AlignTop)

    def addSpace(self):
        spacingWidget = QLabel(self.scrollAreaWidgetContent)
        spacingWidget.setMinimumHeight(20)
        self.verticalLayout.addWidget(spacingWidget)
        self.verticalLayout.setAlignment(spacingWidget, Qt.AlignTop) 

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1101, 681)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.actionUndo = QAction(MainWindow)
        self.actionUndo.setObjectName(u"actionUndo")
        icon = QIcon()
        icon.addFile(u"src/app/images/icon/undo.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionUndo.setIcon(icon)
        self.actionPull = QAction(MainWindow)
        self.actionPull.setObjectName(u"actionPull")
        icon2 = QIcon()
        icon2.addFile(u"src/app/images/icon/pull.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPull.setIcon(icon2)
        self.actionPush = QAction(MainWindow)
        self.actionPush.setObjectName(u"actionPush")
        icon3 = QIcon()
        icon3.addFile(u"src/app/images/icon/push.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPush.setIcon(icon3)
        self.actionStash = QAction(MainWindow)
        self.actionStash.setObjectName(u"actionStash")
        icon4 = QIcon()
        icon4.addFile(u"src/app/images/icon/stash.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStash.setIcon(icon4)
        self.actionPop = QAction(MainWindow)
        self.actionPop.setObjectName(u"actionPop")
        icon5 = QIcon()
        icon5.addFile(u"src/app/images/icon/pop.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPop.setIcon(icon5)
        self.actionBranch = QAction(MainWindow)
        self.actionBranch.setObjectName(u"actionBranch")
        icon6 = QIcon()
        icon6.addFile(u"src/app/images/icon/branch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBranch.setIcon(icon6)
        self.actionUndo_2 = QAction(MainWindow)
        self.actionUndo_2.setObjectName(u"actionUndo_2")
        self.actionUndo_2.setIcon(icon)

        self.actionCommit = QAction(MainWindow)
        self.actionCommit.setObjectName(u"actionCommit")
        iconCommit = QIcon()
        iconCommit.addFile(u"src/app/images/icon/commit.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionCommit.setIcon(iconCommit)

        self.actionAdd = QAction(MainWindow)
        self.actionAdd.setObjectName(u"actionAdd")
        iconAdd = QIcon()
        iconAdd.addFile(u"src/app/images/icon/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAdd.setIcon(iconAdd)

        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        
        font = QFont()
        font.setPointSize(16)
        
        self.verticalLine = QFrame(self.centralwidget)
        self.verticalLine.setObjectName(u"verticalLine")
        self.verticalLine.setGeometry(QRect(250, 0, 21, 631))
        self.verticalLine.setFrameShape(QFrame.VLine)
        self.verticalLine.setFrameShadow(QFrame.Sunken)
        self.horizontalLineSidebar = QFrame(self.centralwidget)
        self.horizontalLineSidebar.setObjectName(u"horizontalLineSidebar")
        self.horizontalLineSidebar.setGeometry(QRect(0, 240, 261, 16))
        self.horizontalLineSidebar.setFrameShape(QFrame.HLine)
        self.horizontalLineSidebar.setFrameShadow(QFrame.Sunken)
        
        self.gitUiPixmap = QPixmap("src/app/images/logo-small.png")
        self.userIcon = QLabel(self.centralwidget)
        self.userIcon.setObjectName(u"userIcon")
        self.userIcon.setGeometry(QRect(10, 92, 235, 80))
        self.userIcon.setPixmap(self.gitUiPixmap)
        self.userIcon.setScaledContents(1)
        
        icon8 = QIcon()
        icon8.addFile(u"src/app/images/teamManagement.png", QSize(), QIcon.Normal, QIcon.Off)
        
        icon9 = QIcon()
        icon9.addFile(u"src/app/images/settings.png", QSize(), QIcon.Normal, QIcon.Off)
        
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(-1, 0, 261, 641))
        self.frame.setStyleSheet(u"background-image: url(src/app/images/sidebar.jpg);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)

        self.frame2 = QFrame(self.centralwidget)
        self.frame2.setGeometry(QRect(260, 0, 841, 641))

        self.userFullname = QLabel(self.centralwidget)
        self.userFullname.setObjectName(u"userFullname")
        self.userFullname.setGeometry(QRect(20, 210, 221, 20))
        font1 = QFont()
        font1.setPointSize(20)
        self.userFullname.setFont(font1)
        self.userFullname.setAlignment(Qt.AlignCenter)
        
        icon10 = QIcon()
        icon10.addFile(u"src/app/images/baseline_date_range_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)

        icon11 = QIcon()
        icon11.addFile(u"src/app/images/baseline_file_copy_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)


        self.teamButton = QPushButton(self.centralwidget)
        self.teamButton.setObjectName(u"teamButton")
        self.teamButton.setGeometry(QRect(10, 260, 241, 61))
        self.teamButton.setFont(font)
        self.teamButton.setIcon(icon8)
        self.teamButton.setIconSize(QSize(32, 32))

        self.teamManagementButton = QPushButton(self.centralwidget)
        self.teamManagementButton.setObjectName(u"teamManagmentButton")
        self.teamManagementButton.setGeometry(QRect(10, 330, 241, 61))
        self.teamManagementButton.setFont(font)
        self.teamManagementButton.setIconSize(QSize(32, 32))
        self.teamManagementButton.setIcon(icon10)

        self.branchButton = QPushButton(self.centralwidget)
        self.branchButton.setObjectName(u"branchButton")
        self.branchButton.setGeometry(QRect(10, 400, 241, 61))
        self.branchButton.setFont(font)
        self.branchButton.setIcon(icon6)
        self.branchButton.setIconSize(QSize(32, 32))

        self.settingsButton = QPushButton(self.centralwidget)
        self.settingsButton.setObjectName(u"settingsButton")
        self.settingsButton.setGeometry(QRect(10, 540, 241, 61))
        self.settingsButton.setFont(font)
        self.settingsButton.setIcon(icon9)
        self.settingsButton.setIconSize(QSize(32, 32))


        self.compareButton = QPushButton(self.centralwidget)
        self.compareButton.setObjectName(u"compareButton")
        self.compareButton.setGeometry(QRect(10, 470, 241, 61))
        self.compareButton.setFont(font)
        self.compareButton.setIcon(icon11)
        self.compareButton.setIconSize(QSize(32, 32))

        self.logGraphFrame = QFrame(self.centralwidget)
        self.logGraphFrame.setGeometry(QRect(260, 0, 841, 641))
        self.logGraphArea = LogGraphArea(parent = self.logGraphFrame)

        MainWindow.setCentralWidget(self.centralwidget)
        self.frame.raise_()
        self.userFullname.raise_()
        self.branchButton.raise_()
        self.verticalLine.raise_()
        self.horizontalLineSidebar.raise_()
        self.teamManagementButton.raise_()
        self.userIcon.raise_()
        self.teamButton.raise_()
        self.settingsButton.raise_()
        self.compareButton.raise_()
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setEnabled(True)
        self.toolBar.setLayoutDirection(Qt.LeftToRight)
        self.toolBar.setAutoFillBackground(False)
        self.toolBar.setStyleSheet(u"spacing: 10px;")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        leftSpacer = QWidget()
        leftSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        rightSpacer = QWidget()
        rightSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.toolBar.addWidget(leftSpacer)
        self.toolBar.addAction(self.actionUndo_2)
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionCommit)
        self.toolBar.addAction(self.actionPull)
        self.toolBar.addAction(self.actionPush)
        self.toolBar.addAction(self.actionBranch)
        self.toolBar.addAction(self.actionStash)
        self.toolBar.addAction(self.actionPop)
        self.toolBar.addWidget(rightSpacer)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Join Team", None))
        self.actionUndo.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionAdd.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.actionCommit.setText(QCoreApplication.translate("MainWindow", u"Commit", None))
        self.actionPull.setText(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPull.setToolTip(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPush.setText(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionPush.setToolTip(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionStash.setText(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionStash.setToolTip(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionPop.setText(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionPop.setToolTip(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionBranch.setText(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionBranch.setToolTip(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionUndo_2.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionUndo_2.setToolTip(QCoreApplication.translate("MainWindow", u"Undo", None))

        self.branchButton.setText(QCoreApplication.translate("MainWindow", u"Branch Management", None))
        self.teamManagementButton.setText(QCoreApplication.translate("MainWindow", u"Team Management", None))
        self.userIcon.setText("")
        self.teamButton.setText(QCoreApplication.translate("MainWindow", u"Team board", None))
        self.settingsButton.setText(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.userFullname.setText(QCoreApplication.translate("MainWindow", u"Pokin Chaitanasakul", None))
        self.compareButton.setText(QCoreApplication.translate("MainWindow", u"Compare files", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))

class SourceTreePage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

    def updateAction(self, action):
        if self.isOpen:
            self.pushNotification("Action: (" + action + ") done successfully")

    def pushNotification(self, text):
        try:
            import pync

            pync.notify(text, title="GitUI")
        except:
            from win10toast import ToastNotifier

            toast = ToastNotifier()
            toast.show_toast("GitUI", text, duration=0)

    def showLogGraph(self):
        try:
            repository = self.data['user'].getRepository()
            
            graph = repository.log_graph().split("\n")
            endResult = "<pre>" 
            i = 0

            while(i < len(graph)):
                endLoop = False

                while(True):
                    if i >= len(graph):
                        endLoop = True
                        break
                    elif "-" not in graph[i]:
                        endResult += "<font color='#F1F33A'>" + graph[i] + "</font><pre>"
                        i += 1
                    else:
                        break
                
                if endLoop:
                    break

                commit = graph[i]
                name = graph[i + 1]
                splitByDash = graph[i].split("-")
                commitSplitByLeftBracket = commit.split("(")

                part1 = "<font color='#F1F33A'>" + splitByDash[0] + "</font><font color='#B2F8FF'>" + splitByDash[1]

                result = part1.split("(")[0] + "</font>" + "<font color='#55FACD'>"

                for j in range(1, len(commitSplitByLeftBracket)):
                    result += ("(" + commitSplitByLeftBracket[j])

                result += "</font>"
                result2 = "<pre><font color='#F1F33A'>" + name + "</font><pre>"

                endResult += (result)
                endResult += (result2 + "\n")
                i += 2

            logGraphLabel = QLabel(endResult)
            logGraphArea = self.ui.logGraphArea

            for i in reversed(range(logGraphArea.verticalLayout.count())):
                    logGraphArea.verticalLayout.itemAt(i).widget().setParent(None)

            logGraphArea.setLogGraph(logGraphLabel)
            return True
        except:
            self._showMessageBox("An error occured")
            return False

    def _showMessageBox(self, text):
        msgBox = QMessageBox(self)

        msgBox.setText(text)
        msgBox.show()
