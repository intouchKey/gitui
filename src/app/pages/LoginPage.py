import sys

from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtMultimedia import *

from . import LoginPageUi as page

class LoginPage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = page.Ui_MainWindow()
        self.ui.setupUi(self)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

        self.ui.pushButton.clicked.connect(self.login)
        self.ui.pushButton_2.clicked.connect(self.register)

    def login(self):
        try:
            username = self.ui.lineEdit.text()
            password = self.ui.lineEdit_2.text()

            data = self.accountController.loginAccount(username, password)

            if data["error"]:
                raise Exception(data['msg'])

            QSound.play("src/app/sounds/correct.wav")

            self.data['actionStack'] = []
            self.data['redoStack'] = []

            data = self.userController.getUser(username)
            self.data['user'] = data['user']
            self.data['user'].toString()

            self.close()
            self._decorateUsername(username)

            if self.data['user'].getRepository() == None: 
                self.data["pages"]["HomePage"].isOpen = True
                self.data["pages"]["HomePage"].show()
            elif self.data['user'].getTeamName() != None:
                self.data["pages"]["TeamBoardPage"].isOpen = True
                self.data["pages"]["TeamBoardPage"].updateHistoryForDashboard()
                self.data["pages"]["TeamBoardPage"].show()
            else:
                self.data["pages"]["TeamOpPage"].isOpen = True
                self.data["pages"]["TeamOpPage"].show()

        except Exception as e:
            print(e)
            msgBox = QMessageBox(self)

            msgBox.setText(str(e))
            msgBox.show()
    
    def register(self):
        self.close()
        nextPage = self.data["pages"]["RegisterPage"]
        nextPage.show()
    
    def _decorateUsername(self, username):
        for pageName in self.data["pages"].keys():
            window = self.data["pages"][pageName]

            if (
                "LoginPage" not in str(window) and
                "RegisterPage" not in str(window) and
                "firstPage" not in str(window)
            ):
                window.ui.userFullname.setText(username)
