from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

from entities.Repository import Repository

class SettingPage(QMainWindow):
    def __init__(self, accountController, teamController, userController):
        QMainWindow.__init__(self, None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.accountController = accountController
        self.teamController = teamController
        self.userController = userController

        repositoryForm = self.ui.repositoryForm
        gitAddForm = self.ui.gitAddSettingForm
        notificationForm = self.ui.notificationSettingForm
        fetchSettingForm = self.ui.fetchSettingForm

        repositoryForm.optionButton1.clicked.connect(self._changeRepository)
        repositoryForm.optionButton2.clicked.connect(self._viewRepository)
        gitAddForm.optionButton1.clicked.connect(self._setAllFiles)
        gitAddForm.optionButton2.clicked.connect(self._setSpecificFile)
        notificationForm.optionButton1.clicked.connect(self._setNotificationToYes)
        notificationForm.optionButton2.clicked.connect(self._setNotificationToNo)
        fetchSettingForm.optionButton1.clicked.connect(self._setFetchToYes)
        fetchSettingForm.optionButton2.clicked.connect(self._setFetchToNo)

    def updateAction(self, action):
        if self.isOpen:
            self.pushNotification("Action: (" + action + ") done successfully")

    def pushNotification(self, text):
        try:
            import pync

            pync.notify(text, title="GitUI")
        except:
            from win10toast import ToastNotifier

            toast = ToastNotifier()
            toast.show_toast("GitUI", text, duration=0)

    def _changeRepository(self):
        text, ok = QInputDialog().getText(
            self,
            "Change repository dialog",
            "Do you want to clear your local history?\n" +
            "* yes\n" +
            "* no",
            QLineEdit.Normal,
            ""
        )

        if not ok:
            return
        elif text != "yes" and text != "no":
            self._showMessageBox("Invalid input.")
        elif text == "yes":
            self.data['user'].clearHistory()
            
            result = self.userController.updateUser(self.data['user'])
            if result['error']:
                self._showMessageBox(self, result['msg'])

        directoryPath = QFileDialog.getExistingDirectory()

        try:
            repository = Repository(directoryPath)

            success = self.data['user'].setRepository(repository)

            if not success:
                return self._showMessageBox("An error occured.")

            result = self.userController.updateUser(self.data['user'])

            if result['error']:
                return self._showMessageBox("An error occured.")

            return self._showMessageBox("Repository change success: " + str(repository.dir()))

        except Exception as e:
            print(e)
            return self._showMessageBox("The directory you chose is not a repository.")

    def _viewRepository(self):
        repository = self.data['user'].getRepository()
        workingTreeDir = repository.dir()

        return self._showMessageBox("Repository dir: " + str(workingTreeDir))

    def _setAllFiles(self):
        gitAddForm = self.ui.gitAddSettingForm
        setting = self.data['user'].getSetting()
        setting.setCommitAllFiles(True)
        self.data['user'].setSetting(setting)

        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        gitAddForm.optionButton1.setStyleSheet("background-color: green; border-radius: 10px")
        gitAddForm.optionButton2.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")

    def _setSpecificFile(self):
        gitAddForm = self.ui.gitAddSettingForm
        setting = self.data['user'].getSetting()
        setting.setCommitAllFiles(False)
        self.data['user'].setSetting(setting)

        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        gitAddForm.optionButton1.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")
        gitAddForm.optionButton2.setStyleSheet("background-color: green; border-radius: 10px")

    def _setNotificationToYes(self):
        notificationForm = self.ui.notificationSettingForm
        setting = self.data['user'].getSetting()
        setting.setNotification(True)

        self.data['user'].setSetting(setting)

        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        notificationForm.optionButton1.setStyleSheet("background-color: green; border-radius: 10px")
        notificationForm.optionButton2.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")

    def _setNotificationToNo(self):
        notificationForm = self.ui.notificationSettingForm
        setting = self.data['user'].getSetting()
        setting.setNotification(False)

        self.data['user'].setSetting(setting)

        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        notificationForm.optionButton1.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")
        notificationForm.optionButton2.setStyleSheet("background-color: green; border-radius: 10px")

    def _setFetchToYes(self):
        fetchSettingForm = self.ui.fetchSettingForm

        setting = self.data['user'].getSetting()
        setting.setFetch(True)

        self.data['user'].setSetting(setting)

        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        fetchSettingForm.optionButton1.setStyleSheet("background-color: green; border-radius: 10px")
        fetchSettingForm.optionButton2.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")

    def _setFetchToNo(self):
        fetchSettingForm = self.ui.fetchSettingForm

        setting = self.data['user'].getSetting()
        setting.setFetch(True)

        self.data['user'].setSetting(setting)
        result = self.userController.updateUser(self.data['user'])

        if result['error']:
            return self._showMessageBox("An error occured.")

        fetchSettingForm.optionButton1.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")
        fetchSettingForm.optionButton2.setStyleSheet("background-color: green; border-radius: 10px")

    def _showMessageBox(self, text):
        msgBox = QMessageBox(self)

        msgBox.setText(text)
        msgBox.show()

class SettingBoxForm(object):
    def __init__(self, settingName, option1, option2, settingWidget):
        self.settingName = settingName
        self.option1 = option1
        self.option2 = option2
        self.widget = settingWidget

    def setupUi(self):
        if self.widget.objectName():
            self.widget.setObjectName(u"Form")
        self.widget.resize(839, 100)
        self.frame = QFrame(self.widget)
        self.frame.resize(839, 100)

        font1 = QFont()
        font1.setPointSize(12)
        font2 = QFont()
        font2.setPointSize(18)
        
        
        self.settingLabel = QLabel(self.widget)
        self.settingLabel.setText(self.settingName)
        self.settingLabel.setGeometry(QRect(40, 35, 141, 31))
        self.settingLabel.setFont(font2)
        self.settingLabel.setStyleSheet("color: white")

        self.optionButton1 = QPushButton(self.option1, self.widget)
        self.optionButton1.setGeometry(QRect(200, 35, 141, 31))
        self.optionButton1.setFont(font1)
        self.optionButton1.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")
        self.optionButton2 = QPushButton(self.option2, self.widget)
        self.optionButton2.setGeometry(QRect(366, 35, 141, 31))
        self.optionButton2.setFont(font1)
        self.optionButton2.setStyleSheet("background-color: #d9d9d9; border-radius: 10px")
        

        self.widget.lower()

        self.retranslateUi(self.widget)

        QMetaObject.connectSlotsByName(self.widget)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))

class SettingArea(QWidget):
    def __init__(self, parent, settingWidgets):
        self.colors = ["#1c3464", "#034569", "#235b79", "#086ca2", "#3c9dd0"]
        self.settingWidgets = settingWidgets
        QWidget.__init__(self, parent)
        self.resize(841, 641)
        
        self.scrollAreaWidgetContent = QWidget()
        self.scrollAreaWidgetContent.setGeometry(QRect(0, 0, 841, 639))

        self.verticalLayout = QVBoxLayout()
        
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setAlignment(Qt.AlignTop)
        
        for i in range(len(self.settingWidgets)):
            currentSettingWidget = self.settingWidgets[i]
            currentSettingWidget.setMinimumSize(839, 100)
            styleSheet = "background-color: " + self.colors[i%4]
            currentSettingWidget.setStyleSheet(styleSheet)
            currentSettingWidget.setParent(self.scrollAreaWidgetContent)
            
            self.verticalLayout.addWidget(currentSettingWidget)
            self.verticalLayout.setAlignment(currentSettingWidget, Qt.AlignTop)

        self.scrollAreaWidgetContent.setLayout(self.verticalLayout)

        self.scrollAreaWidgetContent.setLayout(self.verticalLayout)

        self.scrollArea = QScrollArea()
        self.scrollArea.setGeometry(QRect(0, 0, 841, 641))
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setContentsMargins(0, 0, 0, 0)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollAreaWidgetContent)
        
        layout = QVBoxLayout(self)
        layout.addWidget(self.scrollArea)
        layout.setContentsMargins(0,0,0,0)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1101, 681)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.actionAdd = QAction(MainWindow)
        self.actionAdd.setObjectName(u"actionAdd")
        iconAdd = QIcon()
        iconAdd.addFile(u"src/app/images/icon/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAdd.setIcon(iconAdd)

        self.actionCommit = QAction(MainWindow)
        self.actionCommit.setObjectName(u"actionCommit")
        iconCommit = QIcon()
        iconCommit.addFile(u"src/app/images/icon/commit.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionCommit.setIcon(iconCommit)

        self.actionUndo = QAction(MainWindow)
        self.actionUndo.setObjectName(u"actionUndo")
        icon = QIcon()
        icon.addFile(u"src/app/images/icon/undo.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionUndo.setIcon(icon)
        self.actionPull = QAction(MainWindow)
        self.actionPull.setObjectName(u"actionPull")
        icon2 = QIcon()
        icon2.addFile(u"src/app/images/icon/pull.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPull.setIcon(icon2)
        self.actionPush = QAction(MainWindow)
        self.actionPush.setObjectName(u"actionPush")
        icon3 = QIcon()
        icon3.addFile(u"src/app/images/icon/push.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPush.setIcon(icon3)
        self.actionStash = QAction(MainWindow)
        self.actionStash.setObjectName(u"actionStash")
        icon4 = QIcon()
        icon4.addFile(u"src/app/images/icon/stash.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStash.setIcon(icon4)
        self.actionPop = QAction(MainWindow)
        self.actionPop.setObjectName(u"actionPop")
        icon5 = QIcon()
        icon5.addFile(u"src/app/images/icon/pop.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionPop.setIcon(icon5)
        self.actionBranch = QAction(MainWindow)
        self.actionBranch.setObjectName(u"actionBranch")
        icon6 = QIcon()
        icon6.addFile(u"src/app/images/icon/branch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBranch.setIcon(icon6)
        self.actionUndo_2 = QAction(MainWindow)
        self.actionUndo_2.setObjectName(u"actionUndo_2")
        self.actionUndo_2.setIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.branchButton = QPushButton(self.centralwidget)
        self.branchButton.setObjectName(u"branchButton")
        self.branchButton.setGeometry(QRect(10, 400, 241, 61))
        font = QFont()
        font.setPointSize(16)
        self.branchButton.setFont(font)
        self.branchButton.setIcon(icon6)
        self.branchButton.setIconSize(QSize(32, 32))

        icon11 = QIcon()
        icon11.addFile(u"src/app/images/baseline_file_copy_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)
        
        self.compareButton = QPushButton("Compare files", self.centralwidget)
        self.compareButton.setFont(font)
        self.compareButton.setObjectName(u"branchButton")
        self.compareButton.setGeometry(QRect(10, 470, 241, 61))
        self.compareButton.setIcon(icon11)
        self.compareButton.setIconSize(QSize(32, 32))

        self.verticalLine = QFrame(self.centralwidget)
        self.verticalLine.setObjectName(u"verticalLine")
        self.verticalLine.setGeometry(QRect(250, 0, 21, 631))
        self.verticalLine.setFrameShape(QFrame.VLine)
        self.verticalLine.setFrameShadow(QFrame.Sunken)
        self.horizontalLineSidebar = QFrame(self.centralwidget)
        self.horizontalLineSidebar.setObjectName(u"horizontalLineSidebar")
        self.horizontalLineSidebar.setGeometry(QRect(0, 240, 261, 16))
        self.horizontalLineSidebar.setFrameShape(QFrame.HLine)
        self.horizontalLineSidebar.setFrameShadow(QFrame.Sunken)
        icon10 = QIcon()
        icon10.addFile(u"src/app/images/baseline_date_range_black_18dp.png", QSize(), QIcon.Normal, QIcon.Off)
        self.teamManagementButton = QPushButton(self.centralwidget)
        self.teamManagementButton.setObjectName(u"teamManagementButton")
        self.teamManagementButton.setGeometry(QRect(10, 330, 241, 61))
        self.teamManagementButton.setFont(font)
        self.teamManagementButton.setIconSize(QSize(32, 32))
        self.teamManagementButton.setIcon(icon10)
        self.teamButton = QPushButton(self.centralwidget)
        self.teamButton.setObjectName(u"teamButton")
        self.teamButton.setGeometry(QRect(10, 260, 241, 61))
        self.teamButton.setFont(font)
        icon8 = QIcon()
        icon8.addFile(u"src/app/images/teamManagement.png", QSize(), QIcon.Normal, QIcon.Off)
        self.teamButton.setIcon(icon8)
        self.teamButton.setIconSize(QSize(32, 32))
        self.settingsButton = QPushButton(self.centralwidget)
        self.settingsButton.setObjectName(u"settingsButton")
        self.settingsButton.setGeometry(QRect(10, 540, 241, 61))
        self.settingsButton.setFont(font)
        icon9 = QIcon()
        icon9.addFile(u"src/app/images/settings.png", QSize(), QIcon.Normal, QIcon.Off)
        self.settingsButton.setIcon(icon9)
        self.settingsButton.setIconSize(QSize(32, 32))
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(-1, 0, 261, 641))
        self.frame.setStyleSheet(u"background-image: url(src/app/images/sidebar.jpg);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gitUiPixmap = QPixmap("src/app/images/logo-small.png")
        self.userIcon = QLabel(self.centralwidget)
        self.userIcon.setObjectName(u"userIcon")
        self.userIcon.setGeometry(QRect(10, 92, 235, 80))
        self.userIcon.setPixmap(self.gitUiPixmap)
        self.userIcon.setScaledContents(1)
        self.userFullname = QLabel(self.centralwidget)
        self.userFullname.setObjectName(u"userFullname")
        self.userFullname.setGeometry(QRect(20, 210, 221, 20))
        font1 = QFont()
        font1.setPointSize(20)
        self.userFullname.setFont(font1)
        self.userFullname.setAlignment(Qt.AlignCenter)

        self.frame2 = QFrame(self.centralwidget)
        self.frame2.setGeometry(QRect(260, 0, 841, 641))

        self.repositorySetting = QWidget()
        self.repositoryForm = SettingBoxForm(settingName = "Repository", option1 = "change", option2 = "view", settingWidget = self.repositorySetting)
        self.repositoryForm.setupUi()

        self.gitAddSetting = QWidget()
        self.gitAddSettingForm = SettingBoxForm(settingName = "Git add", option1 = "All files", option2 = "Specific files", settingWidget = self.gitAddSetting)
        self.gitAddSettingForm.setupUi()

        self.notificationSetting = QWidget()
        self.notificationSettingForm = SettingBoxForm(settingName = "Notification", option1 = "Yes", option2 = "No", settingWidget = self.notificationSetting)
        self.notificationSettingForm.setupUi()

        self.fetchSetting = QWidget()
        self.fetchSettingForm = SettingBoxForm(settingName = "Fetch", option1 = "Yes", option2 = "No", settingWidget = self.fetchSetting)
        self.fetchSettingForm.setupUi()

        self.settings = [self.repositorySetting, self.gitAddSetting, self.notificationSetting, self.fetchSetting]
        
        self.settingArea = SettingArea(self.frame2, self.settings)
        
        MainWindow.setCentralWidget(self.centralwidget)

        self.frame.raise_()
        self.branchButton.raise_()
        self.verticalLine.raise_()
        self.horizontalLineSidebar.raise_()
        self.teamManagementButton.raise_()
        self.userIcon.raise_()
        self.teamButton.raise_()
        self.settingsButton.raise_()
        self.compareButton.raise_()
        self.userFullname.raise_()
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setEnabled(True)
        self.toolBar.setLayoutDirection(Qt.LeftToRight)
        self.toolBar.setAutoFillBackground(False)
        self.toolBar.setStyleSheet(u"spacing: 10px;")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        leftSpacer = QWidget()
        leftSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        rightSpacer = QWidget()
        rightSpacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.toolBar.addWidget(leftSpacer)
        self.toolBar.addAction(self.actionUndo_2)
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionCommit)
        self.toolBar.addAction(self.actionPull)
        self.toolBar.addAction(self.actionPush)
        self.toolBar.addAction(self.actionBranch)
        self.toolBar.addAction(self.actionStash)
        self.toolBar.addAction(self.actionPop)
        self.toolBar.addWidget(rightSpacer)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"GitUI - Setting Page", None))
        self.actionUndo.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionAdd.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.actionCommit.setText(QCoreApplication.translate("MainWindow", u"Commit", None))
        self.actionPull.setText(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPull.setToolTip(QCoreApplication.translate("MainWindow", u"Pull", None))
        self.actionPush.setText(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionPush.setToolTip(QCoreApplication.translate("MainWindow", u"Push", None))
        self.actionStash.setText(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionStash.setToolTip(QCoreApplication.translate("MainWindow", u"Stash", None))
        self.actionPop.setText(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionPop.setToolTip(QCoreApplication.translate("MainWindow", u"Pop", None))
        self.actionBranch.setText(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionBranch.setToolTip(QCoreApplication.translate("MainWindow", u"Branch", None))
        self.actionUndo_2.setText(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.actionUndo_2.setToolTip(QCoreApplication.translate("MainWindow", u"Undo", None))
        self.branchButton.setText(QCoreApplication.translate("MainWindow", u"Branch Management", None))
        self.teamManagementButton.setText(QCoreApplication.translate("MainWindow", u"Team Management", None))
        self.userIcon.setText("")
        self.teamButton.setText(QCoreApplication.translate("MainWindow", u"Team board", None))
        self.settingsButton.setText(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))