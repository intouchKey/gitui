import persistent

from .File import File

class History(persistent.Persistent):
    def __init__(self, operationName, developerName, time):
        self._operationName = operationName
        self._developerName = developerName
        self._modifiedTime = time
        self._filesModified = []
    
    def getOperationName(self):
        return self._operationName
    
    def getDeveloperName(self):
        return self._developerName
    
    def getModifiedTime(self):
        return self._modifiedTime
    
    def getFilesModified(self):
        return self._filesModified
    
    def getPathOfFilesModified(self):
        result = []

        for file in self._filesModified:
            result.append(file.getPath())
            
        return result

    def setOperationName(self, operationName):
        self._operationName = operationName
    
    def setDeveloperName(self, developerName):
        self._developerName = developerName
    
    def setModifiedTime(self, modifiedTime):
        self._modifiedTime = modifiedTime
    
    def addFileModified(self, file):
        self._filesModified.append(file)
        self._p_changed = 1
    
    def toString(self):
        print(
                "History:\n"
                + "\t_operationName: " +  str(self._operationName) + "\n"
                + "\t_developerName: " +  str(self._developerName) + "\n"
                + "\t_modifiedTime: " +  str(self._modifiedTime) + "\n"
                + "\t_filesModified: " +  str(self._filesModified) + "\n"
            )