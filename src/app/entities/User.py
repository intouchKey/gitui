import persistent

from entities.Setting import Setting
from entities.Repository import Repository

class User(persistent.Persistent):
    def __init__(self, username, email):
        self._username = username
        self._email = email
        self._setting = Setting()
        self._repository = None
        self._teamName = None
        self._history = []
        self._isLeader = False
    
    def getUsername(self):
        return self._username

    def getEmail(self):
        return self._email
    
    def getSetting(self):
        return self._setting
    
    def getRepository(self):
        return self._repository

    def getTeamName(self):
        return self._teamName
    
    def getHistory(self):
        return self._history
    
    def getIsLeader(self):
        return self._isLeader
    
    def clearHistory(self):
        self._history = []
        
    def setTeamName(self, teamName):
        self._teamName = teamName

    def setSetting(self, setting):
        if type(setting) == Setting:
            self._setting = setting
            return True
        else:
            return False
    
    def setRepository(self, repository):
        if type(repository) == Repository: 
            self._repository = repository
            return True
        else:
            return False
    
    def setIsLeader(self, isLeader):
        self._isLeader = isLeader
    
    def getLatestHistory(self):
        try:
            return self._history[-1]
        except:
            return []
    
    def addHistory(self, history):
        self._history.append(history)
        self._p_changed = 1

    def hasTeam(self):
        return self._teamName != None
    
    def toString(self):
        print(
                "User:\n"
                + "\t_username: " +  str(self._username) + "\n"
                + "\t_email: " +  str(self._email) + "\n"
                + "\t_setting: " +  str(self._setting) + "\n"
                + "\t_repository: " +  str(self._repository) + "\n"
                + "\t_teamName: " +  str(self._teamName) + "\n"
                + "\t_history: " +  str(self._history) + "\n"
                + "\t_isLeader: " +  str(self._isLeader) + "\n"
            )
