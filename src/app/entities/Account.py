import persistent

class Account(persistent.Persistent):
    def __init__(self, username, password, email, verificationCode):
        self._username = username
        self._password = password
        self._email = email
        self._verificationCode = verificationCode
        self._verified = False
    
    def getUsername(self):
        return self._username

    def getPassword(self):
        return self._password

    def getEmail(self):
        return self._email

    def getVerified(self):
        return self._verified
    
    def verify(self, code):
        if (code == self._verificationCode):
            self._verified = True
            
            return True
        return False
