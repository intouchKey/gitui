import persistent

from entities.User import User

class Team(persistent.Persistent):
    def __init__(self, name, password):
        self._name = name
        self._password = password
        self._users = []

    def getName(self):
        return self._name

    def getPassword(self):
        return self._password

    def getUsers(self):
        return self._users

    def setName(self, name):
        self._name = name

    def verifyPassword(self, password):
        if password == self._password:
            return True
        return False

    def addUser(self, user):
        self._users.append(user)
        self._p_changed = 1
    
    def removeUser(self, userToBeRemoved):
        for i in range(len(self._users)):
            if (self._users[i].getUsername() == userToBeRemoved.getUsername()):
                del self._users[i]
                self._p_changed = 1
                
                return dict({ "msg": "Successfully remove user", "error": False })
            
        return dict({ "msg": "User is not found in class", "error": True })

    def toString(self):
        print(
                "Team:\n"
                + "\t_name: " +  str(self._name) + "\n"
                + "\t_password: " +  str(self._password) + "\n"
                + "\t_users: " +  str(self._users) + "\n"
            )