import persistent

class File(persistent.Persistent):
    def __init__(self, name, path):
        self._name = name
        self._path = path
    
    def getName(self):
        return self._name
    
    def getPath(self):
        return self._path
    
    def setName(self, name):
        self._name = name
    
    def setPath(self, path):
        self._path = path
    
    def toString(self):
        print(
                "File:\n"
                + "\t_name: " +  str(self._name) + "\n"
                + "\t_path: " +  str(self._path) + "\n"
            )