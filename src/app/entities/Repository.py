import persistent

from modules.GitRepo import GitRepo

class Repository(GitRepo, persistent.Persistent):
    def __init__(self, path):
        super().__init__(path)