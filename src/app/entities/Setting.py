import persistent

class Setting(persistent.Persistent):
    def __init__(self):
        self._commitAllFiles = True
        self._notification = True
        self._fetch = True
    
    def setCommitAllFiles(self, setting):
        self._commitAllFiles = setting
    
    def setNotification(self, notification):
        self._notification = notification
    
    def setFetch(self, fetch):
        self._fetch = fetch
    
    def getFetch(self):
        return self._fetch
    
    def getCommitAllFiles(self):
        return self._commitAllFiles
    
    def getNotification(self):
        return self._notification
    
    def toString(self):
        print(
                "Setting:\n"
                + "\t_commitAllFiles: " +  str(self._commitAllFiles) + "\n"
                + "\t_notification: " +  str(self._notification) + "\n"
                + "\t_fetch: " +  str(self._fetch) + "\n"
            )