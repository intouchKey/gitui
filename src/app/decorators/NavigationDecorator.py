from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

class NavigationDecorator:
    def decorate(window):
        ui = window.ui
        
        if (
            "LoginPage" not in str(window) and
            "RegisterPage" not in str(window)
        ):
            ui.teamButton.clicked.connect(lambda: NavigationDecorator._toTeamBoardPage(window))
            ui.teamManagementButton.clicked.connect(lambda: NavigationDecorator._toTeamOpPage(window))
            ui.branchButton.clicked.connect(lambda: NavigationDecorator._toBranchManagementPage(window))
            ui.settingsButton.clicked.connect(lambda: NavigationDecorator._toSettingPage(window))
            ui.compareButton.clicked.connect(lambda: NavigationDecorator._toCompareFilesPage(window))
        
        if (
            "TeamOperationPage" in str(window)
        ):
            ui.createTeamButton.clicked.connect(lambda: NavigationDecorator._toCreateTeamPage(window))
            ui.joinTeamButton.clicked.connect(lambda: NavigationDecorator._toJoinTeamPage(window))
            ui.leaveTeamButton.clicked.connect(lambda: NavigationDecorator._leaveTeam(window))
            ui.teamMemberButton.clicked.connect(lambda: NavigationDecorator._toTeamMemberPage(window))

        if (
            "JoinTeamPage" in str(window)
        ):
            ui.joinButton.clicked.connect(lambda: NavigationDecorator._joinTeam(window))
        
        if (
            "CreateTeamPage" in str(window)
        ):
            ui.createButton.clicked.connect(lambda: NavigationDecorator._createTeam(window))

        if (
            "BranchManagementPage" in str(window)
        ):
            ui.sourceTreeButton.clicked.connect(lambda: NavigationDecorator._toSourceTreePage(window))

    def _toTeamOpPage(window):
        if not NavigationDecorator._isRepositoryAvailable(window): 
            return NavigationDecorator._showMessageBox(window, "Repository not chosen")

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["TeamOpPage"].isOpen = True
        window.data["pages"]["TeamOpPage"].show()
    
    def _toCreateTeamPage(window):
        if NavigationDecorator._hasTeam(window):
            return NavigationDecorator._showMessageBox(window, "User already has a team. Please leave the team to create new team.")
        
        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["CreatTeamPage"].isOpen = True
        window.data["pages"]["CreatTeamPage"].show()
    
    def _toJoinTeamPage(window):
        if NavigationDecorator._hasTeam(window):
            return NavigationDecorator._showMessageBox(window, "User already has a team. Please leave the team to join new team.")

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["JoinTeamPage"].isOpen = True
        window.data["pages"]["JoinTeamPage"].show()

    def _toTeamMemberPage(window):
        print("_toTeamMemberPage")
        if not NavigationDecorator._hasTeam(window) or not NavigationDecorator._isLeader(window):
            return NavigationDecorator._showMessageBox(window, "User doesn't has a team or isn't a team leader")

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["TeamMemberPage"].isOpen = True
        page = window.data["pages"]["TeamMemberPage"]
        page.updateTeamMember()
        page.show()

    def _toTeamBoardPage(window):
        if not NavigationDecorator._isRepositoryAvailable(window):
            return NavigationDecorator._showMessageBox(window, "Repository not chosen")

        if not NavigationDecorator._hasTeam(window):
            window.close()
            window.data["pages"]["TeamOpPage"].show()

            return NavigationDecorator._showMessageBox(window, "You don't have a team")
        
        page = window.data["pages"]["TeamBoardPage"]
        page.updateHistoryForDashboard()
        window.isOpen = False
        window.close()
        window.data["pages"]["TeamBoardPage"].isOpen = True
        window.data["pages"]["TeamBoardPage"].show()

    def _toBranchManagementPage(window):
        if not NavigationDecorator._isRepositoryAvailable(window): 
            return NavigationDecorator._showMessageBox(window, "Repository not chosen")

        repository = window.data['user'].getRepository()
        branches = repository.branches()

        page = window.data["pages"]["BranchManagementPage"]

        page.ui.sourceBranch.clear()
        page.ui.destinationBranch.clear()
        page.ui.sourceBranch.addItems(branches)
        page.ui.destinationBranch.addItems(branches)

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["BranchManagementPage"].isOpen = True
        window.data["pages"]["BranchManagementPage"].show()

    def _toCompareFilesPage(window):
        if not NavigationDecorator._isRepositoryAvailable(window): 
            return NavigationDecorator._showMessageBox(window, "Repository not chosen")

        repository = window.data['user'].getRepository()

        try:
            commits = repository.getFormattedCommits().keys()
        except:
            return self._showMessageBox("Invalid directory")

        page = window.data["pages"]["compareFilesPage"]

        page.ui.commits.clear()
        page.ui.commits.addItems(commits)

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["compareFilesPage"].isOpen = True
        window.data["pages"]["compareFilesPage"].show()
    
    def _toSourceTreePage(window):
        page = window.data["pages"]["SourceTreePage"]
        success = page.showLogGraph()

        if not success:
            return

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["SourceTreePage"].isOpen = True
        window.data["pages"]["SourceTreePage"].show()

    def _toSettingPage(window):
        if not NavigationDecorator._isRepositoryAvailable(window): 
            return NavigationDecorator._showMessageBox(window, "Repository not chosen")

        page = window.data["pages"]["SettingPage"]
        setting = window.data['user'].getSetting()
        ui = page.ui
        gitAddForm = ui.gitAddSettingForm
        notificationForm = ui.notificationSettingForm
        fetchForm = ui.fetchSettingForm

        if setting.getCommitAllFiles():
            gitAddForm.optionButton1.setStyleSheet("background-color: green;border-radius: 10px;")
        else:
            gitAddForm.optionButton2.setStyleSheet("background-color: green;border-radius: 10px;")
        
        if setting.getNotification():
            notificationForm.optionButton1.setStyleSheet("background-color: green;border-radius: 10px;")
        else:
            notificationForm.optionButton2.setStyleSheet("background-color: green;border-radius: 10px;")

        if setting.getFetch():
            fetchForm.optionButton1.setStyleSheet("background-color: green;border-radius: 10px;")
        else:
            fetchForm.optionButton2.setStyleSheet("background-color: green;border-radius: 10px;")

        window.data["pages"]["TeamBoardPage"].windowClose = True
        window.isOpen = False
        window.close()
        window.data["pages"]["SettingPage"].isOpen = True
        page = window.data["pages"]["SettingPage"].show()

    def _joinTeam(window):
        window.joinTeam()
    
    def _createTeam(window):
        window.createTeam()
    
    def _leaveTeam(window):
        if NavigationDecorator._isLeader(window):
            window.leaveWholeTeam()
        else:
            window.leaveTeam()
    
    def _isRepositoryAvailable(window):
        data = window.data

        if data['user'].getRepository() == None:
            return False
        
        return True
    
    def _hasTeam(window):
        window.userController.sync()

        return window.data["user"].hasTeam()

    def _isLeader(window):
        return window.data["user"].getIsLeader()

    def _showMessageBox(window, text):
        msgBox = QMessageBox(window)

        msgBox.setText(text)
        msgBox.show()
