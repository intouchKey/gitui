from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from entities.Action import Action

class ToolbarDecorator:
    def decorate(window):
        ui = window.ui

        if (
            "LoginPage" not in str(window) and
            "RegisterPage" not in str(window) and
            "firstPage" not in str(window)
        ):
            ui.actionUndo_2.triggered.connect(lambda: ToolbarDecorator._performUndo(window))
            ui.actionAdd.triggered.connect(lambda: ToolbarDecorator._performAdd(window))
            ui.actionCommit.triggered.connect(lambda: ToolbarDecorator._performCommit(window))
            ui.actionPull.triggered.connect(lambda: ToolbarDecorator._performPull(window))
            ui.actionPush.triggered.connect(lambda: ToolbarDecorator._performPush(window))
            ui.actionBranch.triggered.connect(lambda: ToolbarDecorator._performBranch(window))
            ui.actionStash.triggered.connect(lambda: ToolbarDecorator._performStash(window))
            ui.actionPop.triggered.connect(lambda: ToolbarDecorator._performPop(window))
        
        if (
            "BranchManagementPage" in str(window)
        ):
            ui.mergeButton.clicked.connect(lambda: ToolbarDecorator._performMerge(window))
    
    def _performMerge(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")
        
        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")

        sourceBranch = window.ui.sourceBranch.currentText()
        destinationBranch = window.ui.destinationBranch.currentText()

        if (sourceBranch == destinationBranch):
            return ToolbarDecorator._showMessageBox(window, "Same branch is chosen")

        text, ok = QInputDialog().getText(
            window, 
            "Merge input dialog",
            "This action can't be reverted\n\n" +
            "Enter one of these options\n" +
            "* 'merge' to merge commit\n" +
            "* 'merge and push' to merge and push to remote:",
            QLineEdit.Normal, 
            ""
        )

        if not ok:
            return
        elif text != "merge" and text != "merge and push":
            return ToolbarDecorator._showMessageBox(window, "Invalid input")

        try:
            repository.checkout(destinationBranch)
            result = repository.merge(sourceBranch)

            if result['error']:
                raise Exception(result['msg'])

            if text == "merge and push":
                result = repository.push()

            if result['error']:
                result = repository.push(True)

            if result['error']:
                raise Exception(result['msg'])
            
            return ToolbarDecorator._showMessageBox(window, "Merge files ( " + sourceBranch + " --> " + destinationBranch + " ) successfully")
        except Exception as e:
            return ToolbarDecorator._showMessageBox(window, str(e))

    def _performAdd(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")

        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")
                
        workingTreeDir = repository.dir()
        result = None

        if setting.getCommitAllFiles():
            result = repository.add()
        else:
            filename = QFileDialog.getOpenFileName(
                parent=window, 
                caption='Open file', 
                dir=workingTreeDir,
            )

            if filename[0] == '':
                return

            if workingTreeDir not in filename[0]:
                return ToolbarDecorator._showMessageBox(window, "File not in working directory.")

            result = repository.add(filename[0])

        if not result['error']:
            window.data['actionStack'].append(Action("add"))
            
            currentUser = window.data['user']
            filesWithPath = repository.getUntrackedFilePaths()
            developerName = window.data['user'].getUsername()
            window.userController.updateHistoryAttribute(currentUser, "Add", developerName, filesWithPath)

            return
         
        ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performCommit(window):
        if not ToolbarDecorator._isRepositoryAvailable(window):
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")

        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")

        text, ok = QInputDialog().getText(window, "Commit text input dialog", "Commit text:", QLineEdit.Normal, "")

        if (not text) or (not ok):
            return

        result = repository.commit(text)

        if not result['error']:
            window.data['actionStack'].append(Action("commit"))

            currentUser = window.data['user']
            historyList = currentUser.getHistory()
            n = len(historyList)

            currentFiles = dict()
            for i in range(n - 1, -1, -1):
                if historyList[i].getOperationName() != "Add":
                    break
                else:
                    for file in historyList[i].getFilesModified():
                        currentFiles[file.getName()] = file.getPath()

            developerName = window.data['user'].getUsername()
            window.userController.updateHistoryAttribute(currentUser, "Commit", developerName, currentFiles)

            return

        ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performUndo(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")

        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")
        
        actionStack = window.data['actionStack']
        redoStack = window.data['redoStack']

        if len(actionStack) == 0:
            return ToolbarDecorator._showMessageBox(window, "No action to undo")

        action = actionStack.pop()
        redoStack.append(action)

        try:
            if (action.name == "add"):
                result = repository.unAdd()

                if result['error']: raise Exception(result['msg'])
                return ToolbarDecorator._showMessageBox(window, "Undo action add files successfully")
            elif (action.name == "commit"):
                result = repository.unCommit()

                if result['error']: raise Exception(result['msg'])
                return ToolbarDecorator._showMessageBox(window, "Undo action commit files successfully")
            elif (action.name == "push"):
                result = None
                text, ok = QInputDialog().getText(
                    window, 
                    "Undo push action input dialog", 
                    "You will lose changes in your last commit.\n" +
                    "This action can't be reverted\n\n" +
                    "Enter one of these options\n" +
                    "* 'revert' to revert commit\n" +
                    "* 'revert and push' to revert and push to remote:",
                    QLineEdit.Normal, 
                    ""
                )

                if (not ok):
                    raise Exception("Operation cancelled")

                if text == 'revert':
                    result = repository.unPush()
                elif text == 'revert and push':
                    result = repository.unPush()
                    repository.push()
                else:
                    raise Exception("Invalid input")

                if result['error']: 
                    raise Exception(result['msg'])

                window.data['actionStack'] = []
                window.data['redoStack'] = []

                return ToolbarDecorator._showMessageBox(window, "Undo action push files successfully")
            elif (action.name == "stash"):
                result = repository.stash(True)

                if result['error']: raise Exception(result['msg'])
                return ToolbarDecorator._showMessageBox(window, "Undo stash files successfully")
            elif (action.name == "pop"):
                result = repository.stash()

                if result['error']: raise Exception(result['msg'])
                return ToolbarDecorator._showMessageBox(window, "Undo pop files successfully")
            else:
                raise Exception("Last action: " + action.name + "\n\n This undo action is not supported.")
        except Exception as e:
            action = redoStack.pop()
            actionStack.append(action)

            return ToolbarDecorator._showMessageBox(window, str(e))

    def _performPull(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")
        
        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")
        
        result = repository.pull()

        if not result['error']:
            window.data['actionStack'].append(Action("pull"))

            return ToolbarDecorator._showMessageBox(window, "Pull files successfully")
        return ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performPush(window):
        if not ToolbarDecorator._isRepositoryAvailable(window):
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")
        
        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")
        
        result = repository.push()

        if not result['error']:
            window.data['actionStack'].append(Action("push"))

            currentUser = window.data['user']
            historyList = currentUser.getHistory()
            n = len(historyList)

            currentFiles = dict()
            for i in range(n - 1, -1, -1):
                if historyList[i].getOperationName() == "Push":
                    break
                else:
                    for file in historyList[i].getFilesModified():
                        currentFiles[file.getName()] = file.getPath()

            developerName = window.data['user'].getUsername()
            window.userController.updateHistoryAttribute(currentUser, "Push", developerName, currentFiles)

            return
        elif 'no upstream branch' in result['msg']:
            result = repository.push(True)

            if not result['error']:
                window.data['actionStack'].append(Action("push"))

                currentUser = window.data['user']
                historyList = currentUser.getHistory()
                n = len(historyList)

                currentFiles = dict()
                for i in range(n - 1, -1, -1):
                    if historyList[i].getOperationName() == "Push":
                        break
                    else:
                        for file in historyList[i].getFilesModified():
                            currentFiles[file.getName()] = file.getPath()

                developerName = window.data['user'].getUsername()
                window.userController.updateHistoryAttribute(currentUser, "Push", developerName, currentFiles)

                return

        return ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performBranch(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")
        
        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")

        activeBranch = repository.activeBranch()
        repository.fetch()
        branches = repository.branches(True)

        text, ok = QInputDialog().getText(
            window, 
            "Checkout branch input dialog", 
            "Branches:\n{} \nBranch name (new features/something if it is a new branch):".format(
                '\n'.join(branches) + '\n'
            ), 
            QLineEdit.Normal, 
            ""
        )
        inputList = text.split(' ')
        result = None

        if not ok:
            return

        if len(inputList) == 1:
            result = repository.checkout(inputList[0])
        elif len(inputList) == 2:
            result = repository.checkout(inputList[1], True)
        else:
            return ToolbarDecorator._showMessageBox(window, "Invalid input")
        
        if not result['error']:
            window.data['actionStack'].append(Action("checkout", activeBranch))

            if len(inputList) == 1: return ToolbarDecorator._showMessageBox(window, "Checkout to branch " + inputList[0] + " successfully")
            else: return ToolbarDecorator._showMessageBox(window, "Checkout to branch " + inputList[1] + " successfully")
        return ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performStash(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")
        
        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")

        result = repository.stash()

        if not result['error']:
            window.data['actionStack'].append(Action("stash"))
            return ToolbarDecorator._showMessageBox(window, "Stash files successfully")
        return ToolbarDecorator._showMessageBox(window, result['msg'])

    def _performPop(window):
        if not ToolbarDecorator._isRepositoryAvailable(window): 
            return ToolbarDecorator._showMessageBox(window, "Repository not chosen")

        setting = window.data['user'].getSetting()
        repository = window.data['user'].getRepository()

        if setting.getFetch():
            try:
                repository.fetch()
            except:
                return ToolbarDecorator._showMessageBox(window, "Invalid directory")
        
        result = repository.stash(True)

        if not result['error']:
            window.data['actionStack'].append(Action("pop"))
            return ToolbarDecorator._showMessageBox(window, "Pop files successfully")
        return ToolbarDecorator._showMessageBox(window, result['msg'])

    def _isRepositoryAvailable(window):
        data = window.data

        if data['user'].getRepository() == None:
            return False
        
        return True
    
    def _showMessageBox(window, text):
        msgBox = QMessageBox(window)

        msgBox.setText(text)
        msgBox.show()
