import sys
import pymsgbox

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from pages.CreateTeamPage import CreateTeamPage
from pages.HomePage import HomePage
from pages.JoinTeamPage import JoinTeamPage
from pages.LoginPage import LoginPage
from pages.TeamOperationPage import TeamOpPage
from pages.RegisterPage import RegisterPage
from pages.TeamBoardPage import TeamBoardPage
from pages.BranchManagementPage import BranchManagementPage
from pages.SettingPage import SettingPage
from pages.CompareFilesPage import CompareFilesPage
from pages.SourceTreePage import SourceTreePage
from pages.TeamMemberPage import TeamMemberPage

from modules.DatabaseConnection import openConnection

from decorators.NavigationDecorator import NavigationDecorator
from decorators.ToolbarDecorator import ToolbarDecorator

from controllers.AccountController import AccountController
from controllers.TeamController import TeamController
from controllers.UserController import UserController

from service.GitUISystem import GitUISystem

def createApp():
    app = QApplication(sys.argv)
    gitUISystem = GitUISystem()

    accountController = AccountController()
    teamController = TeamController()
    userController = UserController(gitUISystem)

    createTeamPage = CreateTeamPage(accountController, teamController, userController)
    homePage = HomePage(accountController, teamController, userController)
    joinTeamPage = JoinTeamPage(accountController, teamController, userController)
    teamOpPage = TeamOpPage(accountController, teamController, userController)
    loginPage = LoginPage(accountController, teamController, userController)
    registerPage = RegisterPage(accountController, teamController, userController)
    teamBoardPage = TeamBoardPage(accountController, teamController, userController)
    branchManagementPage = BranchManagementPage(accountController, teamController, userController)
    settingPage = SettingPage(accountController, teamController, userController)
    compareFilesPage = CompareFilesPage(accountController, teamController, userController)
    sourceTreePage = SourceTreePage(accountController, teamController, userController)
    teamMemberPage = TeamMemberPage(accountController, teamController, userController)

    data = {
        "pages": {
            "CreatTeamPage": createTeamPage, 
            "HomePage": homePage, 
            "JoinTeamPage": joinTeamPage, 
            "TeamOpPage": teamOpPage,
            "LoginPage": loginPage,
            "RegisterPage": registerPage,
            "TeamBoardPage": teamBoardPage,
            "BranchManagementPage": branchManagementPage,
            "SettingPage": settingPage,
            "compareFilesPage": compareFilesPage,
            "SourceTreePage": sourceTreePage,
            "TeamMemberPage": teamMemberPage
        }
    }

    for pageName in data["pages"].keys():
        page = data["pages"][pageName]
        page.data = data
        page.isOpen = False
        
        if pageName != "LoginPage" and pageName != "RegisterPage":
            gitUISystem.addObserver(page)
        
        NavigationDecorator.decorate(page)
        ToolbarDecorator.decorate(page)

    loginPage.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    connectionError = False

    try:
        openConnection()
    except:
        connectionError = True
        pymsgbox.alert("Please try again later.", title="Connection Error")

    if not connectionError:
        createApp()