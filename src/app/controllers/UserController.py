from repositories.UserRepository import UserRepository

from entities.File import File
from entities.History import History
from entities.User import User

from datetime import datetime

class UserController:
    def __init__(self, gitUISystem):
        self.userRepository = UserRepository()
        self.gitUISystem = gitUISystem

    def createUser(self, username, email):
        data = self.userRepository.saveUser(username, email)

        if (data['error'] == False):
            return data
        
        return dict({ "msg": "An error occured", "error": True })

    def getUser(self, username):
        data = self.userRepository.findUserByUsername(username)

        return data
    
    def updateUser(self, user):
        if type(user) != User:
            return dict({ "msg": "An error occured", "error": True })

        self.userRepository.commitChanges(user)

        return dict({ "msg": "User updated", "error": False })
    
    def updateHistoryAttribute(self, user, operationName, developerName, filesNameWithPath):
        if len(filesNameWithPath) == 0:
            if not user.getSetting().getNotification():
                return
            return self.gitUISystem.updateLatestAction(operationName + " - no change")

        currentTime = str(datetime.now())
        newHistory = History(operationName, developerName, currentTime)

        for name, path in filesNameWithPath.items():
            newHistory.addFileModified(File(name, path))

        user.addHistory(newHistory)
        self.userRepository.commitChanges(user)

        if not user.getSetting().getNotification():
            return
            
        self.gitUISystem.updateLatestAction(newHistory._operationName)
    
    def sync(self):
        self.userRepository.sync()
        
 