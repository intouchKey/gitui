from email_validator import validate_email, EmailNotValidError
from repositories.AccountRepository import AccountRepository
from passlib.hash import sha256_crypt
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import smtplib
import ssl
import string
import random

SENDER_EMAIL = 'gitui.verify@gmail.com'
EMAIL_PASSWORD = '2w3e4r5t!'

class AccountController:
    def __init__(self):
        self.accountRepository = AccountRepository()

    def loginAccount(self, username, password):
        data = self.accountRepository.findAccountByUsername(username)

        if data['error']:
            return data

        account = data['result']

        if not account.getVerified():
            return dict({ "msg": "Account not verified", "error": True })

        if not sha256_crypt.verify(password, account.getPassword()):
            return dict({ "msg": "Verification error", "error": True })
        
        return dict({ "msg": "Verification success", "error": False })

        
    def createAccount(self, username, password, email):
        validated = self._validateEmail(email) and self._validateUsername(username) and self._validatePassword(password)

        if (validated):
            hashedPassword = sha256_crypt.encrypt(password)
            verificationCode = self._generateVerificationCode()

            data = self.accountRepository.saveAccount(username, hashedPassword, email, verificationCode)

            if (data['error'] == False):
                self._sendVerificationCode(email, verificationCode)
            
            return data
        
        return dict({ "msg": "Validation error", "error": True })

    def verifyAccount(self, username, verificationCode):
        data = self.accountRepository.findAccountByUsername(username)

        if data['error']:
            return data

        account = data['result']

        if account.getVerified():
            return dict({ "msg": "Account already verified", "error": True })

        verified = account.verify(verificationCode)

        if not verified:
            return dict({ "msg": "Incorrect verification code", "error": True })

        self.accountRepository.commitChanges(account)

        return dict({ "msg": "Account verified", "error": True })
    
    def getVerificationState(self, username):
        data = self.accountRepository.findAccountByUsername(username)

        if data['error']:
            return False

        account = data['result']

        return account.getVerified()
    
    def _generateVerificationCode(self, size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def _sendVerificationCode(self, receiverEmail, verificationCode):
        senderEmail = SENDER_EMAIL
        receiverEmail = receiverEmail
        password = EMAIL_PASSWORD

        html = """\
        <html>
        <body>
            <p>
            <img src="https://img.techpowerup.org/200330/logo425.png">
            </p>
            <p align="center">
                <font size="4" color="#153a63">
                    Your verification code is {}.
                </font>
            </p>
        </body>
        </html>
        """.format(verificationCode)

        message = MIMEMultipart("alternative")
        message["Subject"] = "GitUI verification code"
        message["From"] = senderEmail
        message["To"] = receiverEmail

        htmlPart = MIMEText(html, "html")

        message.attach(htmlPart)

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(senderEmail, password)
            server.sendmail(
                senderEmail, receiverEmail, message.as_string()
            )

    def _validateEmail(self, email):
        try:
            v = validate_email(email)
            email = v["email"]

            return True
        except EmailNotValidError as e:
            return False
    
    def _validateUsername(self, username):
        return True

    def _validatePassword(self, password):
        return len(password) <= 15 and len(password) >= 7 and password.isalnum()