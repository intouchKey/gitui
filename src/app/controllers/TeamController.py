from repositories.TeamRepository import TeamRepository
from repositories.UserRepository import UserRepository
from entities.User import User
from passlib.hash import sha256_crypt

class TeamController:
    def __init__(self):
        self.teamRepository = TeamRepository()

    def createTeam(self, teamName, password, user, userRepository):
        if (user.hasTeam()):
            return dict({ "msg": "User already had team", "error": True })

        validated = self._validateTeamName(teamName) and self._validatePassword(password) and self._validateUser(user)

        if (validated):
            hashedPassword = sha256_crypt.hash(password)
            data = self.teamRepository.saveTeam(teamName, hashedPassword, user)

            if (data['error']):
                return data
            else:
                user.setTeamName(teamName)
                user.setIsLeader(True)
                userRepository.commitChanges(user)
                return data
        
        return dict({ "msg": "Validation error", "error": True })

    def joinTeam(self, teamName, password, user, userRepository):
        if (user.hasTeam()):
            return dict({ "msg": "User already had team", "error": True })

        data = self.teamRepository.findTeamByTeamName(teamName)

        if data['error']:
            return data

        team = data['result']
        
        
        if not sha256_crypt.verify(password, team.getPassword()):
            return dict({ "msg": "Password not matched", "error": True })
        
        team.addUser(user)
        self.teamRepository.commitChanges(team)
        user.setTeamName(teamName)
        userRepository.commitChanges(user)

        return dict({ "msg": "Join team successfully", "error": False })
    
    def leaveTeam(self, teamName, user, userRepository):
        data = self.teamRepository.findTeamByTeamName(teamName)

        if data['error']:
            return data

        team = data['result']

        result = team.removeUser(user)

        if (result['error']):
            return result
        
        self.teamRepository.commitChanges(team)

        user.setTeamName(None)
        userRepository.commitChanges(user)

        return dict({ "msg": "Leave team successfully", "error": False })

    def leaveWholeTeam(self, teamName, userRepository):
        data = self.teamRepository.findTeamByTeamName(teamName)

        if data['error']:
            return data

        team = data['result']

        for member in team.getUsers():
            member.setTeamName(None)
            member.setIsLeader(False)
            userRepository.commitChanges(member)

        result = self.teamRepository.deleteTeam(teamName)

        if (result['error']):
            return result
        else:
            return dict({"msg": "Everyone leave team successfully", "error": False})

    def _validateTeamName(self, teamName):
        return True

    def _validatePassword(self, password):
        return True
    
    def _validateUser(self, user):
        return isinstance(user, User)
